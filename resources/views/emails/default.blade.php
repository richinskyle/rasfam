<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Compax Notification</title>
</head>
 
<body>
{!! $first_name !!},
<br /><br />
{!! $content !!}
<br />
<h4>Direct Link</h4>
<a href="{!! $link !!}" target="_blank">{!! $link_title !!}</a>
<br />
<br />
Do not reply to this email.
</body>
</html>