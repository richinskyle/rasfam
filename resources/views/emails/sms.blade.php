{!! $first_name !!}, here are the results after today's games: 
<br />
<br />
@foreach($brackets as $bracket)
{{ $bracket->username }}: {{ $bracket->points }}; 
@endforeach