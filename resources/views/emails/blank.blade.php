<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Notification</title>
</head>
 
<body>
{!! $first_name !!},
<br /><br />
Here are the results after today's games:
<br />
<br />
@foreach($brackets as $bracket)
{{ $bracket->username }}: {{ $bracket->points }} <br/>
@endforeach
<br/><br/>
DO NOT REPLY TO THIS EMAIL.
</body>
</html>