<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>March Madness Dashboard</title>
    
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="http://www.jumpstartsports.com/upload/images/Radnor_Basketball/448650-basketball__mario_sports_mix_.png">
  
</head>

<body onload="calculatePoints()">
    <div id="wrap">
        <div id="regbar">
            <div id="navthing">
                <h2><a href="logout" id="loginform">Logout</a> | <a href="/dashboard">Dashboard</a></h2>
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="well well-light">
                        <div class="row">
                            <div class="col-md-8">
                                <h1 style="text-decoration: underline;">Active Brackets</h1>
                            </div>
                            <div style="float:right; padding-right:15px; padding-left: 10px;">
                                <a href="/my-bracket/{{$user->username}}"><button class="btn btn-primary">View my bracket</button></a>
                            </div>
                            @if($user->id == 1)
                            <div style="float:right; padding-left: 10px;">
                                <a href="/my-bracket/master"><button class="btn btn-warning">View Master Bracket</button></a>
                            </div>
                            @endif
                            @if($user->id == 1)
                            <div style="float:right;">
                                <button class="btn btn-primary" onclick="resultsEmail()">Send Results Email</button>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @if($my_bracket->submitted == 1)
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Bracket Name</th>
                                            <th>Bracket Owner</th>
                                            <th>Champion</th>
                                            <th>Total Points</th>
                                            <th>Total Possible Points</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($brackets as $bracket)
                                        {!! csrf_field() !!}
                                        <tr>
                                            <td><a href="/my-bracket/{{$bracket->username}}" style="color:#337ab7;">{{$bracket->bracket_name}}</a></td>
                                            <td>{{$bracket->username}}</td>
                                            <td>{{$bracket->champion}}</td>
                                            <td>{{$bracket->points}}</td>
                                            <td>{{$bracket->possible_points}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                                @if($my_bracket->submitted == 0)
                                <p>*You must have a submitted bracket to view active brackets</p>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>

</body>
</html>
