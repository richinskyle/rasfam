<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Dashboard</title>
    
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="image/R.jpeg">
  
</head>

<body>
    <div id="wrap">
        <div id="regbar">
            <div id="navthing">
                <h2><a href="logout" id="loginform">Logout</a></h2>
            </div>
        </div>
    </div>
    <div id="content">
        <div class="col-md-12">
            <div class="col-md-12">
                <h1 style="color:white; text-align: center; margin-top:0px; margin-bottom:20px;">Dashboard</h1>
            </div>
            <div class="col-md-3">
                <a href="/march-madness"><img src="image/March-Madness.jpeg" style="height:100%; width:100%"></a>
            </div>
        </div>
    </div>


    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>

</body>
</html>
