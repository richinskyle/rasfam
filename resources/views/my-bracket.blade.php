<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Pick 'Em Dashboard</title>
    
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/sweetalert.css') }}">
    <link rel="icon" href="http://www.jumpstartsports.com/upload/images/Radnor_Basketball/448650-basketball__mario_sports_mix_.png">
  
</head>
<body onload="calculatePoints()">
<div id="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="well tournament">
                <div class="row">
                    <div class="col-md-12">
                        <span style="float:left;"><a href="/march-madness"><button class="btn btn-primary">Return</button></a></span>
                        <span style="float:right;">Points: {{ $entries['points'] }}</span><br/>
                        <h1 class="bold vertical-align">March Madness 2017</h1>
                        <div class="col-md-2 col-md-offset-5">
                            @if($entries['bracket_name'] == NULL)
                            <input name="bracketName" class="form-control" placeholder="Bracket Name">
                            @endif
                            <br/>
                            @if($entries['bracket_name'] == NULL)
                            <center><button class="btn btn-primary" onclick="submitBracketName()">Submit Bracket Name</button><center>
                            @endif
                            @if($entries['bracket_name'] != NULL)
                            <h3 class="vertical-align">{{ $entries['bracket_name'] }}</h3>
                            @endif
                        </div>
                        <br/><br/><br/><br/>
                        <div class="col-md-2 col-md-offset-5">
                            <div class="vertical-align">
                                <ul class="champion-line round-4">
                                    <li class="spacer">&nbsp;</li>
                                    <br/><br/>
                                    @if($entries['champion'] == NULL || $entries['champion'] != $master_bracket->champion && $master_bracket->champion == NULL)
                                    <li class="champion game-top bold">{{ $entries['champion'] }}</li>
                                    @elseif($entries['champion'] == $master_bracket->champion && $master_bracket->champion != NULL)
                                    <li class="champion game-top bold green-font">{{ $entries['champion'] }}</li>
                                    @elseif($entries['champion'] != $master_bracket->champion && $master_bracket->champion != NULL)
                                    <li class="champion game-top bold red-font">{{ $entries['champion'] }}</li>
                                    @endif
                                    <br/>
                                    <li class="vertical-align">CHAMPION</li>
                                    <br/>
                                    @if($entries['submitted'] != 1)
                                    <li class="vertical-align"><button class="btn btn-primary" onclick="lockBracket()">Lock it in!</button></li>
                                    @endif
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="username" value="{{ $entries['username'] }}">
                                </ul>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <main id="tournament-south">
                            <ul class="round round-1">
                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_1', '{{$south_teams[0]->name}}', '{{$south_teams[15]->name}}')">{{ $south_teams[0]->name }}({{ $south_teams[0]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_1', '{{$south_teams[15]->name}}', '{{$south_teams[0]->name}}')">{{ $south_teams[15]->name }}({{ $south_teams[15]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_2', '{{$south_teams[7]->name}}', '{{$south_teams[8]->name}}')">{{ $south_teams[7]->name }}({{ $south_teams[7]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_2', '{{$south_teams[8]->name}}', '{{$south_teams[7]->name}}')">{{ $south_teams[8]->name }}({{ $south_teams[8]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_3', '{{$south_teams[4]->name}}', '{{$south_teams[11]->name}}')">{{ $south_teams[4]->name }}({{ $south_teams[4]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_3', '{{$south_teams[11]->name}}', '{{$south_teams[4]->name}}')">{{ $south_teams[11]->name }}({{ $south_teams[11]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_4', '{{$south_teams[3]->name}}', '{{$south_teams[12]->name}}')">{{ $south_teams[3]->name }}({{ $south_teams[3]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_4', '{{$south_teams[12]->name}}', '{{$south_teams[3]->name}}')">{{ $south_teams[12]->name }}({{ $south_teams[12]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_5', '{{$south_teams[5]->name}}', '{{$south_teams[10]->name}}')">{{ $south_teams[5]->name }}({{ $south_teams[5]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_5', '{{$south_teams[10]->name}}', '{{$south_teams[5]->name}}')">{{ $south_teams[10]->name }}({{ $south_teams[10]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_6', '{{$south_teams[2]->name}}', '{{$south_teams[13]->name}}')">{{ $south_teams[2]->name }}({{ $south_teams[2]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_6', '{{$south_teams[13]->name}}', '{{$south_teams[2]->name}}')">{{ $south_teams[13]->name }}({{ $south_teams[13]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_7', '{{$south_teams[6]->name}}', '{{$south_teams[9]->name}}')">{{ $south_teams[6]->name }}({{ $south_teams[6]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_7', '{{$south_teams[9]->name}}', '{{$south_teams[6]->name}}')">{{ $south_teams[9]->name }}({{ $south_teams[9]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('south_r64_8', '{{$south_teams[1]->name}}', '{{$south_teams[14]->name}}')">{{ $south_teams[1]->name }}({{ $south_teams[1]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('south_r64_8', '{{$south_teams[14]->name}}', '{{$south_teams[1]->name}}')">{{ $south_teams[14]->name }}({{ $south_teams[14]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-2">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['south_r64_1'] == NULL || $entries['south_r64_1'] != $master_bracket->south_r64_1 && $master_bracket->south_r64_1 == NULL)
                                <li class="game game-top" onclick="setEntry('south_r32_1', '{{$entries['south_r64_1']}}', '{{$entries['south_r64_2']}}')">{{ $entries['south_r64_1'] }} <span></span></li>
                                @elseif($entries['south_r64_1'] == $master_bracket->south_r64_1 && $master_bracket->south_r64_1 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('south_r32_1', '{{$entries['south_r64_1']}}', '{{$entries['south_r64_2']}}')">{{ $entries['south_r64_1'] }} <span></span></li>
                                @elseif($entries['south_r64_1'] != $master_bracket->south_r64_1 && $master_bracket->south_r64_1 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('south_r32_1', '{{$entries['south_r64_1']}}', '{{$entries['south_r64_2']}}')">{{ $entries['south_r64_1'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['south_r64_2'] == NULL || $entries['south_r64_2'] != $master_bracket->south_r64_2 && $master_bracket->south_r64_2 == NULL)
                                <li class="game game-bottom" onclick="setEntry('south_r32_1', '{{$entries['south_r64_2']}}', '{{$entries['south_r64_1']}}')">{{ $entries['south_r64_2'] }} <span></span></li>
                                @elseif($entries['south_r64_2'] == $master_bracket->south_r64_2 && $master_bracket->south_r64_2 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('south_r32_1', '{{$entries['south_r64_2']}}', '{{$entries['south_r64_1']}}')">{{ $entries['south_r64_2'] }} <span></span></li>
                                @elseif($entries['south_r64_2'] != $master_bracket->south_r64_2 && $master_bracket->south_r64_2 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('south_r32_1', '{{$entries['south_r64_2']}}', '{{$entries['south_r64_1']}}')">{{ $entries['south_r64_2'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['south_r64_3'] == NULL || $entries['south_r64_3'] != $master_bracket->south_r64_3 && $master_bracket->south_r64_3 == NULL)
                                <li class="game game-top" onclick="setEntry('south_r32_2', '{{$entries['south_r64_3']}}', '{{$entries['south_r64_4']}}')">{{ $entries['south_r64_3'] }} <span></span></li>
                                @elseif($entries['south_r64_3'] == $master_bracket->south_r64_3 && $master_bracket->south_r64_3 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('south_r32_2', '{{$entries['south_r64_3']}}', '{{$entries['south_r64_4']}}')">{{ $entries['south_r64_3'] }} <span></span></li>
                                @elseif($entries['south_r64_3'] != $master_bracket->south_r64_3 && $master_bracket->south_r64_3 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('south_r32_2', '{{$entries['south_r64_3']}}', '{{$entries['south_r64_4']}}')">{{ $entries['south_r64_3'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['south_r64_4'] == NULL || $entries['south_r64_4'] != $master_bracket->south_r64_4 && $master_bracket->south_r64_4 == NULL)
                                <li class="game game-bottom" onclick="setEntry('south_r32_2', '{{$entries['south_r64_4']}}', '{{$entries['south_r64_3']}}')">{{ $entries['south_r64_4'] }} <span></span></li>
                                @elseif($entries['south_r64_4'] == $master_bracket->south_r64_4 && $master_bracket->south_r64_4 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('south_r32_2', '{{$entries['south_r64_4']}}', '{{$entries['south_r64_3']}}')">{{ $entries['south_r64_4'] }} <span></span></li>
                                @elseif($entries['south_r64_4'] != $master_bracket->south_r64_4 && $master_bracket->south_r64_4 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('south_r32_2', '{{$entries['south_r64_4']}}', '{{$entries['south_r64_3']}}')">{{ $entries['south_r64_4'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['south_r64_5'] == NULL || $entries['south_r64_5'] != $master_bracket->south_r64_5 && $master_bracket->south_r64_5 == NULL)
                                <li class="game game-top" onclick="setEntry('south_r32_3', '{{$entries['south_r64_5']}}', '{{$entries['south_r64_6']}}')">{{ $entries['south_r64_5'] }} <span></span></li>
                                @elseif($entries['south_r64_5'] == $master_bracket->south_r64_5 && $master_bracket->south_r64_5 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('south_r32_3', '{{$entries['south_r64_5']}}', '{{$entries['south_r64_6']}}')">{{ $entries['south_r64_5'] }} <span></span></li>
                                @elseif($entries['south_r64_5'] != $master_bracket->south_r64_5 && $master_bracket->south_r64_5 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('south_r32_3', '{{$entries['south_r64_5']}}', '{{$entries['south_r64_6']}}')">{{ $entries['south_r64_5'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['south_r64_6'] == NULL || $entries['south_r64_6'] != $master_bracket->south_r64_6 && $master_bracket->south_r64_6 == NULL)
                                <li class="game game-bottom" onclick="setEntry('south_r32_3', '{{$entries['south_r64_6']}}', '{{$entries['south_r64_5']}}')">{{ $entries['south_r64_6'] }} <span></span></li>
                                @elseif($entries['south_r64_6'] == $master_bracket->south_r64_6 && $master_bracket->south_r64_6 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('south_r32_3', '{{$entries['south_r64_6']}}', '{{$entries['south_r64_5']}}')">{{ $entries['south_r64_6'] }} <span></span></li>
                                @elseif($entries['south_r64_6'] != $master_bracket->south_r64_6 && $master_bracket->south_r64_6 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('south_r32_3', '{{$entries['south_r64_6']}}', '{{$entries['south_r64_5']}}')">{{ $entries['south_r64_6'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['south_r64_7'] == NULL || $entries['south_r64_7'] != $master_bracket->south_r64_7 && $master_bracket->south_r64_7 == NULL)
                                <li class="game game-top" onclick="setEntry('south_r32_4', '{{$entries['south_r64_7']}}', '{{$entries['south_r64_8']}}')">{{ $entries['south_r64_7'] }} <span></span></li>
                                @elseif($entries['south_r64_7'] == $master_bracket->south_r64_7 && $master_bracket->south_r64_7 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('south_r32_4', '{{$entries['south_r64_7']}}', '{{$entries['south_r64_8']}}')">{{ $entries['south_r64_7'] }} <span></span></li>
                                @elseif($entries['south_r64_7'] != $master_bracket->south_r64_7 && $master_bracket->south_r64_7 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('south_r32_4', '{{$entries['south_r64_7']}}', '{{$entries['south_r64_8']}}')">{{ $entries['south_r64_7'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['south_r64_8'] == NULL || $entries['south_r64_8'] != $master_bracket->south_r64_8 && $master_bracket->south_r64_8 == NULL)
                                <li class="game game-bottom" onclick="setEntry('south_r32_4', '{{$entries['south_r64_8']}}', '{{$entries['south_r64_7']}}')">{{ $entries['south_r64_8'] }} <span></span></li>
                                @elseif($entries['south_r64_8'] == $master_bracket->south_r64_8 && $master_bracket->south_r64_8 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('south_r32_4', '{{$entries['south_r64_8']}}', '{{$entries['south_r64_7']}}')">{{ $entries['south_r64_8'] }} <span></span></li>
                                @elseif($entries['south_r64_8'] != $master_bracket->south_r64_8 && $master_bracket->south_r64_8 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('south_r32_4', '{{$entries['south_r64_8']}}', '{{$entries['south_r64_7']}}')">{{ $entries['south_r64_8'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-3">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['south_r32_1'] == NULL || $entries['south_r32_1'] != $master_bracket->south_r32_1 && $master_bracket->south_r32_1 == NULL)
                                <li class="game game-top" onclick="setEntry('south_r16_1', '{{$entries['south_r32_1']}}', '{{$entries['south_r32_2']}}')">{{ $entries['south_r32_1'] }} <span></span></li>
                                @elseif($entries['south_r32_1'] == $master_bracket->south_r32_1 && $master_bracket->south_r32_1 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('south_r16_1', '{{$entries['south_r32_1']}}', '{{$entries['south_r32_2']}}')">{{ $entries['south_r32_1'] }} <span></span></li>
                                @elseif($entries['south_r32_1'] != $master_bracket->south_r32_1 && $master_bracket->south_r32_1 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('south_r16_1', '{{$entries['south_r32_1']}}', '{{$entries['south_r32_2']}}')">{{ $entries['south_r32_1'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['south_r32_2'] == NULL || $entries['south_r32_2'] != $master_bracket->south_r32_2 && $master_bracket->south_r32_2 == NULL)
                                <li class="game game-bottom" onclick="setEntry('south_r16_1', '{{$entries['south_r32_2']}}', '{{$entries['south_r32_1']}}')">{{ $entries['south_r32_2'] }} <span></span></li>
                                @elseif($entries['south_r32_2'] == $master_bracket->south_r32_2 && $master_bracket->south_r32_2 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('south_r16_1', '{{$entries['south_r32_2']}}', '{{$entries['south_r32_1']}}')">{{ $entries['south_r32_2'] }} <span></span></li>
                                @elseif($entries['south_r32_2'] != $master_bracket->south_r32_2 && $master_bracket->south_r32_2 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('south_r16_1', '{{$entries['south_r32_2']}}', '{{$entries['south_r32_1']}}')">{{ $entries['south_r32_2'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['south_r32_3'] == NULL || $entries['south_r32_3'] != $master_bracket->south_r32_3 && $master_bracket->south_r32_3 == NULL)
                                <li class="game game-top" onclick="setEntry('south_r16_2', '{{$entries['south_r32_3']}}', '{{$entries['south_r32_4']}}')">{{ $entries['south_r32_3'] }} <span></span></li>
                                @elseif($entries['south_r32_3'] == $master_bracket->south_r32_3 && $master_bracket->south_r32_3 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('south_r16_2', '{{$entries['south_r32_3']}}', '{{$entries['south_r32_4']}}')">{{ $entries['south_r32_3'] }} <span></span></li>
                                @elseif($entries['south_r32_3'] != $master_bracket->south_r32_3 && $master_bracket->south_r32_3 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('south_r16_2', '{{$entries['south_r32_3']}}', '{{$entries['south_r32_4']}}')">{{ $entries['south_r32_3'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['south_r32_4'] == NULL || $entries['south_r32_4'] != $master_bracket->south_r32_4 && $master_bracket->south_r32_4 == NULL)
                                <li class="game game-bottom" onclick="setEntry('south_r16_2', '{{$entries['south_r32_4']}}', '{{$entries['south_r32_3']}}')">{{ $entries['south_r32_4'] }} <span></span></li>
                                @elseif($entries['south_r32_4'] == $master_bracket->south_r32_4 && $master_bracket->south_r32_4 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('south_r16_2', '{{$entries['south_r32_4']}}', '{{$entries['south_r32_3']}}')">{{ $entries['south_r32_4'] }} <span></span></li>
                                @elseif($entries['south_r32_4'] != $master_bracket->south_r32_4 && $master_bracket->south_r32_4 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('south_r16_2', '{{$entries['south_r32_4']}}', '{{$entries['south_r32_3']}}')">{{ $entries['south_r32_4'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-4">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['south_r16_1'] == NULL || $entries['south_r16_1'] != $master_bracket->south_r16_1 && $master_bracket->south_r16_1 == NULL)
                                <li class="game game-top" onclick="setEntry('south_r8_1', '{{$entries['south_r16_1']}}', '{{$entries['south_r16_2']}}')">{{ $entries['south_r16_1'] }} <span></span></li>
                                @elseif($entries['south_r16_1'] == $master_bracket->south_r16_1 && $master_bracket->south_r16_1 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('south_r8_1', '{{$entries['south_r16_1']}}', '{{$entries['south_r16_2']}}')">{{ $entries['south_r16_1'] }} <span></span></li>
                                @elseif($entries['south_r16_1'] != $master_bracket->south_r16_1 && $master_bracket->south_r16_1 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('south_r8_1', '{{$entries['south_r16_1']}}', '{{$entries['south_r16_2']}}')">{{ $entries['south_r16_1'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['south_r16_2'] == NULL || $entries['south_r16_2'] != $master_bracket->south_r16_2 && $master_bracket->south_r16_2 == NULL)
                                <li class="game game-bottom" onclick="setEntry('south_r8_1', '{{$entries['south_r16_2']}}', '{{$entries['south_r16_1']}}')">{{ $entries['south_r16_2'] }} <span></span></li>
                                @elseif($entries['south_r16_2'] == $master_bracket->south_r16_2 && $master_bracket->south_r16_2 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('south_r8_1', '{{$entries['south_r16_2']}}', '{{$entries['south_r16_1']}}')">{{ $entries['south_r16_2'] }} <span></span></li>
                                @elseif($entries['south_r16_2'] != $master_bracket->south_r16_2 && $master_bracket->south_r16_2 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('south_r8_1', '{{$entries['south_r16_2']}}', '{{$entries['south_r16_1']}}')">{{ $entries['south_r16_2'] }} <span></span></li>
                                @endif
                                
                                <li class="spacer">&nbsp;</li>
                            </ul>       
                        </main>
                    </div>
                    <div class="col-md-6">
                        <main id="tournament-east">
                           <ul class="round round-4">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['east_r16_1'] == NULL || $entries['east_r16_1'] != $master_bracket->east_r16_1 && $master_bracket->east_r16_1 == NULL)
                                <li class="game-two game-top" onclick="setEntry('east_r8_1', '{{$entries['east_r16_1']}}', '{{$entries['east_r16_2']}}')"> <span>{{ $entries['east_r16_1'] }}</span></li>
                                @elseif($entries['east_r16_1'] == $master_bracket->east_r16_1 && $master_bracket->east_r16_1 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('east_r8_1', '{{$entries['east_r16_1']}}', '{{$entries['east_r16_2']}}')"> <span>{{ $entries['east_r16_1'] }}</span></li>
                                @elseif($entries['east_r16_1'] != $master_bracket->east_r16_1 && $master_bracket->east_r16_1 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('east_r8_1', '{{$entries['east_r16_1']}}', '{{$entries['east_r16_2']}}')"> <span>{{ $entries['east_r16_1'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['east_r16_2'] == NULL || $entries['east_r16_2'] != $master_bracket->east_r16_2 && $master_bracket->east_r16_2 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('east_r8_1', '{{$entries['east_r16_2']}}', '{{$entries['east_r16_1']}}')"> <span>{{ $entries['east_r16_2'] }}</span></li>
                                @elseif($entries['east_r16_2'] == $master_bracket->east_r16_2 && $master_bracket->east_r16_2 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('east_r8_1', '{{$entries['east_r16_2']}}', '{{$entries['east_r16_1']}}')"> <span>{{ $entries['east_r16_2'] }}</span></li>
                                @elseif($entries['east_r16_2'] != $master_bracket->east_r16_2 && $master_bracket->east_r16_2 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('east_r8_1', '{{$entries['east_r16_2']}}', '{{$entries['east_r16_1']}}')"> <span>{{ $entries['east_r16_2'] }}</span></li>
                                @endif
                                
                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-3">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['east_r32_1'] == NULL || $entries['east_r32_1'] != $master_bracket->east_r32_1 && $master_bracket->east_r32_1 == NULL)
                                <li class="game-two game-top" onclick="setEntry('east_r16_1', '{{$entries['east_r32_1']}}', '{{$entries['east_r32_2']}}')"> <span>{{ $entries['east_r32_1'] }}</span></li>
                                @elseif($entries['east_r32_1'] == $master_bracket->east_r32_1 && $master_bracket->east_r32_1 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('east_r16_1', '{{$entries['east_r32_1']}}', '{{$entries['east_r32_2']}}')"> <span>{{ $entries['east_r32_1'] }}</span></li>
                                @elseif($entries['east_r32_1'] != $master_bracket->east_r32_1 && $master_bracket->east_r32_1 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('east_r16_1', '{{$entries['east_r32_1']}}', '{{$entries['east_r32_2']}}')"> <span>{{ $entries['east_r32_1'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['east_r32_2'] == NULL || $entries['east_r32_2'] != $master_bracket->east_r32_2 && $master_bracket->east_r32_2 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('east_r16_1', '{{$entries['east_r32_2']}}', '{{$entries['east_r32_1']}}')"> <span>{{ $entries['east_r32_2'] }}</span></li>
                                @elseif($entries['east_r32_2'] == $master_bracket->east_r32_2 && $master_bracket->east_r32_2 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('east_r16_1', '{{$entries['east_r32_2']}}', '{{$entries['east_r32_1']}}')"> <span>{{ $entries['east_r32_2'] }}</span></li>
                                @elseif($entries['east_r32_2'] != $master_bracket->east_r32_2 && $master_bracket->east_r32_2 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('east_r16_1', '{{$entries['east_r32_2']}}', '{{$entries['east_r32_1']}}')"> <span>{{ $entries['east_r32_2'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['east_r32_3'] == NULL || $entries['east_r32_3'] != $master_bracket->east_r32_3 && $master_bracket->east_r32_3 == NULL)
                                <li class="game-two game-top" onclick="setEntry('east_r16_2', '{{$entries['east_r32_3']}}', '{{$entries['east_r32_4']}}')"> <span>{{ $entries['east_r32_3'] }}</span></li>
                                @elseif($entries['east_r32_3'] == $master_bracket->east_r32_3 && $master_bracket->east_r32_3 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('east_r16_2', '{{$entries['east_r32_3']}}', '{{$entries['east_r32_4']}}')"> <span>{{ $entries['east_r32_3'] }}</span></li>
                                @elseif($entries['east_r32_3'] != $master_bracket->east_r32_3 && $master_bracket->east_r32_3 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('east_r16_2', '{{$entries['east_r32_3']}}', '{{$entries['east_r32_4']}}')"> <span>{{ $entries['east_r32_3'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['east_r32_4'] == NULL || $entries['east_r32_4'] != $master_bracket->east_r32_4 && $master_bracket->east_r32_4 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('east_r16_2', '{{$entries['east_r32_4']}}', '{{$entries['east_r32_3']}}')"> <span>{{ $entries['east_r32_4'] }}</span></li>
                                @elseif($entries['east_r32_4'] == $master_bracket->east_r32_4 && $master_bracket->east_r32_4 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('east_r16_2', '{{$entries['east_r32_4']}}', '{{$entries['east_r32_3']}}')"> <span>{{ $entries['east_r32_4'] }}</span></li>
                                @elseif($entries['east_r32_4'] != $master_bracket->east_r32_4 && $master_bracket->east_r32_4 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('east_r16_2', '{{$entries['east_r32_4']}}', '{{$entries['east_r32_3']}}')"> <span>{{ $entries['east_r32_4'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-2">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['east_r64_1'] == NULL || $entries['east_r64_1'] != $master_bracket->east_r64_1 && $master_bracket->east_r64_1 == NULL)
                                <li class="game-two game-top" onclick="setEntry('east_r32_1', '{{$entries['east_r64_1']}}', '{{$entries['east_r64_2']}}')"> <span>{{ $entries['east_r64_1'] }}</span></li>
                                @elseif($entries['east_r64_1'] == $master_bracket->east_r64_1 && $master_bracket->east_r64_1 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('east_r32_1', '{{$entries['east_r64_1']}}', '{{$entries['east_r64_2']}}')"> <span>{{ $entries['east_r64_1'] }}</span></li>
                                @elseif($entries['east_r64_1'] != $master_bracket->east_r64_1 && $master_bracket->east_r64_1 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('east_r32_1', '{{$entries['east_r64_1']}}', '{{$entries['east_r64_2']}}')"> <span>{{ $entries['east_r64_1'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['east_r64_2'] == NULL || $entries['east_r64_2'] != $master_bracket->east_r64_2 && $master_bracket->east_r64_2 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('east_r32_1', '{{$entries['east_r64_2']}}', '{{$entries['east_r64_1']}}')"> <span>{{ $entries['east_r64_2'] }}</span></li>
                                @elseif($entries['east_r64_2'] == $master_bracket->east_r64_2 && $master_bracket->east_r64_2 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('east_r32_1', '{{$entries['east_r64_2']}}', '{{$entries['east_r64_1']}}')"> <span>{{ $entries['east_r64_2'] }}</span></li>
                                @elseif($entries['east_r64_2'] != $master_bracket->east_r64_2 && $master_bracket->east_r64_2 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('east_r32_1', '{{$entries['east_r64_2']}}', '{{$entries['east_r64_1']}}')"> <span>{{ $entries['east_r64_2'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['east_r64_3'] == NULL || $entries['east_r64_3'] != $master_bracket->east_r64_3 && $master_bracket->east_r64_3 == NULL)
                                <li class="game-two game-top" onclick="setEntry('east_r32_2', '{{$entries['east_r64_3']}}', '{{$entries['east_r64_4']}}')"> <span>{{ $entries['east_r64_3'] }}</span></li>
                                @elseif($entries['east_r64_3'] == $master_bracket->east_r64_3 && $master_bracket->east_r64_3 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('east_r32_2', '{{$entries['east_r64_3']}}', '{{$entries['east_r64_4']}}')"> <span>{{ $entries['east_r64_3'] }}</span></li>
                                @elseif($entries['east_r64_3'] != $master_bracket->east_r64_3 && $master_bracket->east_r64_3 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('east_r32_2', '{{$entries['east_r64_3']}}', '{{$entries['east_r64_4']}}')"> <span>{{ $entries['east_r64_3'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['east_r64_4'] == NULL || $entries['east_r64_4'] != $master_bracket->east_r64_4 && $master_bracket->east_r64_4 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('east_r32_2', '{{$entries['east_r64_4']}}', '{{$entries['east_r64_3']}}')"> <span>{{ $entries['east_r64_4'] }}</span></li>
                                @elseif($entries['east_r64_4'] == $master_bracket->east_r64_4 && $master_bracket->east_r64_4 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('east_r32_2', '{{$entries['east_r64_4']}}', '{{$entries['east_r64_3']}}')"> <span>{{ $entries['east_r64_4'] }}</span></li>
                                @elseif($entries['east_r64_4'] != $master_bracket->east_r64_4 && $master_bracket->east_r64_4 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('east_r32_2', '{{$entries['east_r64_4']}}', '{{$entries['east_r64_3']}}')"> <span>{{ $entries['east_r64_4'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['east_r64_5'] == NULL || $entries['east_r64_5'] != $master_bracket->east_r64_5 && $master_bracket->east_r64_5 == NULL)
                                <li class="game-two game-top" onclick="setEntry('east_r32_3', '{{$entries['east_r64_5']}}', '{{$entries['east_r64_6']}}')"> <span>{{ $entries['east_r64_5'] }}</span></li>
                                @elseif($entries['east_r64_5'] == $master_bracket->east_r64_5 && $master_bracket->east_r64_5 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('east_r32_3', '{{$entries['east_r64_5']}}', '{{$entries['east_r64_6']}}')"> <span>{{ $entries['east_r64_5'] }}</span></li>
                                @elseif($entries['east_r64_5'] != $master_bracket->east_r64_5 && $master_bracket->east_r64_5 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('east_r32_3', '{{$entries['east_r64_5']}}', '{{$entries['east_r64_6']}}')"> <span>{{ $entries['east_r64_5'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['east_r64_6'] == NULL || $entries['east_r64_6'] != $master_bracket->east_r64_6 && $master_bracket->east_r64_6 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('east_r32_3', '{{$entries['east_r64_6']}}', '{{$entries['east_r64_5']}}')"> <span>{{ $entries['east_r64_6'] }}</span></li>
                                @elseif($entries['east_r64_6'] == $master_bracket->east_r64_6 && $master_bracket->east_r64_6 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('east_r32_3', '{{$entries['east_r64_6']}}', '{{$entries['east_r64_5']}}')"> <span>{{ $entries['east_r64_6'] }}</span></li>
                                @elseif($entries['east_r64_6'] != $master_bracket->east_r64_6 && $master_bracket->east_r64_6 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('east_r32_3', '{{$entries['east_r64_6']}}', '{{$entries['east_r64_5']}}')"> <span>{{ $entries['east_r64_6'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['east_r64_7'] == NULL || $entries['east_r64_7'] != $master_bracket->east_r64_7 && $master_bracket->east_r64_7 == NULL)
                                <li class="game-two game-top" onclick="setEntry('east_r32_4', '{{$entries['east_r64_7']}}', '{{$entries['east_r64_8']}}')"> <span>{{ $entries['east_r64_7'] }}</span></li>
                                @elseif($entries['east_r64_7'] == $master_bracket->east_r64_7 && $master_bracket->east_r64_7 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('east_r32_4', '{{$entries['east_r64_7']}}', '{{$entries['east_r64_8']}}')"> <span>{{ $entries['east_r64_7'] }}</span></li>
                                @elseif($entries['east_r64_7'] != $master_bracket->east_r64_7 && $master_bracket->east_r64_7 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('east_r32_4', '{{$entries['east_r64_7']}}', '{{$entries['east_r64_8']}}')"> <span>{{ $entries['east_r64_7'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['east_r64_8'] == NULL || $entries['east_r64_8'] != $master_bracket->east_r64_8 && $master_bracket->east_r64_8 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('east_r32_4', '{{$entries['east_r64_8']}}', '{{$entries['east_r64_7']}}')"> <span>{{ $entries['east_r64_8'] }}</span></li>
                                @elseif($entries['east_r64_8'] == $master_bracket->east_r64_8 && $master_bracket->east_r64_8 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('east_r32_4', '{{$entries['east_r64_8']}}', '{{$entries['east_r64_7']}}')"> <span>{{ $entries['east_r64_8'] }}</span></li>
                                @elseif($entries['east_r64_8'] != $master_bracket->east_r64_8 && $master_bracket->east_r64_8 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('east_r32_4', '{{$entries['east_r64_8']}}', '{{$entries['east_r64_7']}}')"> <span>{{ $entries['east_r64_8'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-1">
                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_1', '{{$east_teams[0]->name}}', '{{$east_teams[15]->name}}')"> <span>({{ $east_teams[0]->seed }}){{ $east_teams[0]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_1', '{{$east_teams[15]->name}}', '{{$east_teams[0]->name}}')"> <span>({{ $east_teams[15]->seed }}){{ $east_teams[15]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_2', '{{$east_teams[7]->name}}', '{{$east_teams[8]->name}}')"> <span>({{ $east_teams[7]->seed }}){{ $east_teams[7]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_2', '{{$east_teams[8]->name}}', '{{$east_teams[7]->name}}')"> <span>({{ $east_teams[8]->seed }}){{ $east_teams[8]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_3', '{{$east_teams[4]->name}}', '{{$east_teams[11]->name}}')"> <span>({{ $east_teams[4]->seed }}){{ $east_teams[4]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_3', '{{$east_teams[11]->name}}', '{{$east_teams[4]->name}}')"> <span>({{ $east_teams[11]->seed }}){{ $east_teams[11]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_4', '{{$east_teams[3]->name}}', '{{$east_teams[12]->name}}')"> <span>({{ $east_teams[3]->seed }}){{ $east_teams[3]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_4', '{{$east_teams[12]->name}}', '{{$east_teams[3]->name}}')"> <span>({{ $east_teams[12]->seed }}){{ $east_teams[12]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_5', '{{$east_teams[5]->name}}', '{{$east_teams[10]->name}}')"> <span>({{ $east_teams[5]->seed }}){{ $east_teams[5]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_5', '{{$east_teams[10]->name}}', '{{$east_teams[5]->name}}')"> <span>({{ $east_teams[10]->seed }}){{ $east_teams[10]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_6', '{{$east_teams[2]->name}}', '{{$east_teams[13]->name}}')"> <span>({{ $east_teams[2]->seed }}){{ $east_teams[2]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_6', '{{$east_teams[13]->name}}', '{{$east_teams[2]->name}}')"> <span>({{ $east_teams[13]->seed }}){{ $east_teams[13]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_7', '{{$east_teams[6]->name}}', '{{$east_teams[9]->name}}')"> <span>({{ $east_teams[6]->seed }}){{ $east_teams[6]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_7', '{{$east_teams[9]->name}}', '{{$east_teams[6]->name}}')"> <span>({{ $east_teams[9]->seed }}){{ $east_teams[9]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('east_r64_8', '{{$east_teams[1]->name}}', '{{$east_teams[14]->name}}')"> <span>({{ $east_teams[1]->seed }}){{ $east_teams[1]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('east_r64_8', '{{$east_teams[14]->name}}', '{{$east_teams[1]->name}}')"> <span>({{ $east_teams[14]->seed }}){{ $east_teams[14]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                            </ul>        
                        </main>
                    </div>
                </div>
                <br/>
                <div class="row col-md-12">
                    <div class="vertical-align">
                        <ul class="champion-line round-4">
                            <li class="spacer">&nbsp;</li>
                            @if($entries['left_champ'] == NULL || $entries['left_champ'] != $master_bracket->left_champ && $master_bracket->left_champ == NULL)                    
                            <li class="game game-top" onclick="setEntry('champion', '{{$entries['left_champ']}}', '{{$entries['right_champ']}}')">{{ $entries['left_champ'] }} <span></span></li>
                            @elseif($entries['left_champ'] == $master_bracket->left_champ && $master_bracket->left_champ != NULL)                    
                            <li class="game game-top green-font" onclick="setEntry('champion', '{{$entries['left_champ']}}', '{{$entries['right_champ']}}')">{{ $entries['left_champ'] }} <span></span></li>
                            @elseif($entries['left_champ'] != $master_bracket->left_champ && $master_bracket->left_champ != NULL)                    
                            <li class="game game-top red-font" onclick="setEntry('champion', '{{$entries['left_champ']}}', '{{$entries['right_champ']}}')">{{ $entries['left_champ'] }} <span></span></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <br/>
                <div class="row col-md-12 vertical-align">
                    <div class="col-md-2">
                        <ul class="final-round round-4">
                            <li class="spacer">&nbsp;</li>
                            
                            @if($entries['south_r8_1'] == NULL || $entries['south_r8_1'] != $master_bracket->south_r8_1 && $master_bracket->south_r8_1 == NULL)
                            <li class="game game-top" onclick="setEntry('left_champ', '{{$entries['south_r8_1']}}', '{{$entries['west_r8_1']}}')">{{ $entries['south_r8_1'] }} <span></span></li>
                            @elseif($entries['south_r8_1'] == $master_bracket->south_r8_1 && $master_bracket->south_r8_1 != NULL)
                            <li class="game game-top green-font" onclick="setEntry('left_champ', '{{$entries['south_r8_1']}}', '{{$entries['west_r8_1']}}')">{{ $entries['south_r8_1'] }} <span></span></li>
                            @elseif($entries['south_r8_1'] != $master_bracket->south_r8_1 && $master_bracket->south_r8_1 != NULL)
                            <li class="game game-top red-font" onclick="setEntry('left_champ', '{{$entries['south_r8_1']}}', '{{$entries['west_r8_1']}}')">{{ $entries['south_r8_1'] }} <span></span></li>
                            @endif
                            <li class="game four-game-spacer">&nbsp;</li>
                            @if($entries['west_r8_1'] == NULL || $entries['west_r8_1'] != $master_bracket->west_r8_1 && $master_bracket->west_r8_1 == NULL)
                            <li class="game game-bottom" onclick="setEntry('left_champ', '{{$entries['west_r8_1']}}', '{{$entries['south_r8_1']}}')">{{ $entries['west_r8_1'] }} <span></span></li>
                            @elseif($entries['west_r8_1'] == $master_bracket->west_r8_1 && $master_bracket->west_r8_1 != NULL)
                            <li class="game game-bottom green-font" onclick="setEntry('left_champ', '{{$entries['west_r8_1']}}', '{{$entries['south_r8_1']}}')">{{ $entries['west_r8_1'] }} <span></span></li>
                            @elseif($entries['west_r8_1'] != $master_bracket->west_r8_1 && $master_bracket->west_r8_1 != NULL)
                            <li class="game game-bottom red-font" onclick="setEntry('left_champ', '{{$entries['west_r8_1']}}', '{{$entries['south_r8_1']}}')">{{ $entries['west_r8_1'] }} <span></span></li>
                            @endif
                            
                            <li class="spacer">&nbsp;</li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <ul class="final-round round-4">
                            <li class="spacer">&nbsp;</li>
                            
                            @if($entries['east_r8_1'] == NULL || $entries['east_r8_1'] != $master_bracket->east_r8_1 && $master_bracket->east_r8_1 == NULL)
                            <li class="game-two game-top" onclick="setEntry('right_champ', '{{$entries['east_r8_1']}}', '{{$entries['midwest_r8_1']}}')"> <span>{{ $entries['east_r8_1'] }}</span></li>
                            @elseif($entries['east_r8_1'] == $master_bracket->east_r8_1 && $master_bracket->east_r8_1 != NULL)
                            <li class="game-two game-top green-font" onclick="setEntry('right_champ', '{{$entries['east_r8_1']}}', '{{$entries['midwest_r8_1']}}')"> <span>{{ $entries['east_r8_1'] }}</span></li>
                            @elseif($entries['east_r8_1'] != $master_bracket->east_r8_1 && $master_bracket->east_r8_1 != NULL)
                            <li class="game-two game-top red-font" onclick="setEntry('right_champ', '{{$entries['east_r8_1']}}', '{{$entries['midwest_r8_1']}}')"> <span>{{ $entries['east_r8_1'] }}</span></li>
                            @endif
                            <li class="game-two four-game-spacer-two">&nbsp;</li>
                            @if($entries['midwest_r8_1'] == NULL || $entries['midwest_r8_1'] != $master_bracket->midwest_r8_1 && $master_bracket->midwest_r8_1 == NULL)
                            <li class="game-two game-bottom" onclick="setEntry('right_champ', '{{$entries['midwest_r8_1']}}', '{{$entries['east_r8_1']}}')"> <span>{{ $entries['midwest_r8_1'] }}</span></li>
                            @elseif($entries['midwest_r8_1'] == $master_bracket->midwest_r8_1 && $master_bracket->midwest_r8_1 != NULL)
                            <li class="game-two game-bottom green-font" onclick="setEntry('right_champ', '{{$entries['midwest_r8_1']}}', '{{$entries['east_r8_1']}}')"> <span>{{ $entries['midwest_r8_1'] }}</span></li>
                            @elseif($entries['midwest_r8_1'] != $master_bracket->midwest_r8_1 && $master_bracket->midwest_r8_1 != NULL)
                            <li class="game-two game-bottom red-font" onclick="setEntry('right_champ', '{{$entries['midwest_r8_1']}}', '{{$entries['east_r8_1']}}')"> <span>{{ $entries['midwest_r8_1'] }}</span></li>
                            @endif
                            
                            <li class="spacer">&nbsp;</li>
                        </ul>
                    </div>
                </div>
                <br/>
                <div class="row col-md-12">
                    <div class="vertical-align">
                        <ul class="champion-line round-4">
                            <li class="spacer">&nbsp;</li>
                            @if($entries['right_champ'] == NULL || $entries['right_champ'] != $master_bracket->right_champ && $master_bracket->right_champ == NULL)                    
                            <li class="game game-top" onclick="setEntry('champion', '{{$entries['right_champ']}}', '{{$entries['left_champ']}}')">{{ $entries['right_champ'] }} <span></span></li>
                            @elseif($entries['right_champ'] == $master_bracket->right_champ && $master_bracket->right_champ != NULL)                    
                            <li class="game game-top green-font" onclick="setEntry('champion', '{{$entries['right_champ']}}', '{{$entries['left_champ']}}')">{{ $entries['right_champ'] }} <span></span></li>
                            @elseif($entries['right_champ'] != $master_bracket->right_champ && $master_bracket->right_champ != NULL)                    
                            <li class="game game-top red-font" onclick="setEntry('champion', '{{$entries['right_champ']}}', '{{$entries['left_champ']}}')">{{ $entries['right_champ'] }} <span></span></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <main id="tournament-west">
                            <ul class="round round-1">
                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_1', '{{$west_teams[0]->name}}', '{{$west_teams[15]->name}}')">{{ $west_teams[0]->name }}({{ $west_teams[0]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_1', '{{$west_teams[15]->name}}', '{{$west_teams[0]->name}}')">{{ $west_teams[15]->name }}({{ $west_teams[15]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_2', '{{$west_teams[7]->name}}', '{{$west_teams[8]->name}}')">{{ $west_teams[7]->name }}({{ $west_teams[7]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_2', '{{$west_teams[8]->name}}', '{{$west_teams[7]->name}}')">{{ $west_teams[8]->name }}({{ $west_teams[8]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_3', '{{$west_teams[4]->name}}', '{{$west_teams[11]->name}}')">{{ $west_teams[4]->name }}({{ $west_teams[4]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_3', '{{$west_teams[11]->name}}', '{{$west_teams[4]->name}}')">{{ $west_teams[11]->name }}({{ $west_teams[11]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_4', '{{$west_teams[3]->name}}', '{{$west_teams[12]->name}}')">{{ $west_teams[3]->name }}({{ $west_teams[3]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_4', '{{$west_teams[12]->name}}', '{{$west_teams[3]->name}}')">{{ $west_teams[12]->name }}({{ $west_teams[12]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_5', '{{$west_teams[5]->name}}', '{{$west_teams[10]->name}}')">{{ $west_teams[5]->name }}({{ $west_teams[5]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_5', '{{$west_teams[10]->name}}', '{{$west_teams[5]->name}}')">{{ $west_teams[10]->name }}({{ $west_teams[10]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_6', '{{$west_teams[2]->name}}', '{{$west_teams[13]->name}}')">{{ $west_teams[2]->name }}({{ $west_teams[2]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_6', '{{$west_teams[13]->name}}', '{{$west_teams[2]->name}}')">{{ $west_teams[13]->name }}({{ $west_teams[13]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_7', '{{$west_teams[6]->name}}', '{{$west_teams[9]->name}}')">{{ $west_teams[6]->name }}({{ $west_teams[6]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_7', '{{$west_teams[9]->name}}', '{{$west_teams[6]->name}}')">{{ $west_teams[9]->name }}({{ $west_teams[9]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game game-top" onclick="setEntry('west_r64_8', '{{$west_teams[1]->name}}', '{{$west_teams[14]->name}}')">{{ $west_teams[1]->name }}({{ $west_teams[1]->seed }}) <span></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom" onclick="setEntry('west_r64_8', '{{$west_teams[14]->name}}', '{{$west_teams[1]->name}}')">{{ $west_teams[14]->name }}({{ $west_teams[14]->seed }}) <span></span></li>

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-2">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['west_r64_1'] == NULL || $entries['west_r64_1'] != $master_bracket->west_r64_1 && $master_bracket->west_r64_1 == NULL)
                                <li class="game game-top" onclick="setEntry('west_r32_1', '{{$entries['west_r64_1']}}', '{{$entries['west_r64_2']}}')">{{ $entries['west_r64_1'] }} <span></span></li>
                                @elseif($entries['west_r64_1'] == $master_bracket->west_r64_1 && $master_bracket->west_r64_1 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('west_r32_1', '{{$entries['west_r64_1']}}', '{{$entries['west_r64_2']}}')">{{ $entries['west_r64_1'] }} <span></span></li>
                                @elseif($entries['west_r64_1'] != $master_bracket->west_r64_1 && $master_bracket->west_r64_1 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('west_r32_1', '{{$entries['west_r64_1']}}', '{{$entries['west_r64_2']}}')">{{ $entries['west_r64_1'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['west_r64_2'] == NULL || $entries['west_r64_2'] != $master_bracket->west_r64_2 && $master_bracket->west_r64_2 == NULL)
                                <li class="game game-bottom" onclick="setEntry('west_r32_1', '{{$entries['west_r64_2']}}', '{{$entries['west_r64_1']}}')">{{ $entries['west_r64_2'] }} <span></span></li>
                                @elseif($entries['west_r64_2'] == $master_bracket->west_r64_2 && $master_bracket->west_r64_2 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('west_r32_1', '{{$entries['west_r64_2']}}', '{{$entries['west_r64_1']}}')">{{ $entries['west_r64_2'] }} <span></span></li>
                                @elseif($entries['west_r64_2'] != $master_bracket->west_r64_2 && $master_bracket->west_r64_2 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('west_r32_1', '{{$entries['west_r64_2']}}', '{{$entries['west_r64_1']}}')">{{ $entries['west_r64_2'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['west_r64_3'] == NULL || $entries['west_r64_3'] != $master_bracket->west_r64_3 && $master_bracket->west_r64_3 == NULL)
                                <li class="game game-top" onclick="setEntry('west_r32_2', '{{$entries['west_r64_3']}}', '{{$entries['west_r64_4']}}')">{{ $entries['west_r64_3'] }} <span></span></li>
                                @elseif($entries['west_r64_3'] == $master_bracket->west_r64_3 && $master_bracket->west_r64_3 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('west_r32_2', '{{$entries['west_r64_3']}}', '{{$entries['west_r64_4']}}')">{{ $entries['west_r64_3'] }} <span></span></li>
                                @elseif($entries['west_r64_3'] != $master_bracket->west_r64_3 && $master_bracket->west_r64_3 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('west_r32_2', '{{$entries['west_r64_3']}}', '{{$entries['west_r64_4']}}')">{{ $entries['west_r64_3'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['west_r64_4'] == NULL || $entries['west_r64_4'] != $master_bracket->west_r64_4 && $master_bracket->west_r64_4 == NULL)
                                <li class="game game-bottom" onclick="setEntry('west_r32_2', '{{$entries['west_r64_4']}}', '{{$entries['west_r64_3']}}')">{{ $entries['west_r64_4'] }} <span></span></li>
                                @elseif($entries['west_r64_4'] == $master_bracket->west_r64_4 && $master_bracket->west_r64_4 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('west_r32_2', '{{$entries['west_r64_4']}}', '{{$entries['west_r64_3']}}')">{{ $entries['west_r64_4'] }} <span></span></li>
                                @elseif($entries['west_r64_4'] != $master_bracket->west_r64_4 && $master_bracket->west_r64_4 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('west_r32_2', '{{$entries['west_r64_4']}}', '{{$entries['west_r64_3']}}')">{{ $entries['west_r64_4'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['west_r64_5'] == NULL || $entries['west_r64_5'] != $master_bracket->west_r64_5 && $master_bracket->west_r64_5 == NULL)
                                <li class="game game-top" onclick="setEntry('west_r32_3', '{{$entries['west_r64_5']}}', '{{$entries['west_r64_6']}}')">{{ $entries['west_r64_5'] }} <span></span></li>
                                @elseif($entries['west_r64_5'] == $master_bracket->west_r64_5 && $master_bracket->west_r64_5 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('west_r32_3', '{{$entries['west_r64_5']}}', '{{$entries['west_r64_6']}}')">{{ $entries['west_r64_5'] }} <span></span></li>
                                @elseif($entries['west_r64_5'] != $master_bracket->west_r64_5 && $master_bracket->west_r64_5 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('west_r32_3', '{{$entries['west_r64_5']}}', '{{$entries['west_r64_6']}}')">{{ $entries['west_r64_5'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['west_r64_6'] == NULL || $entries['west_r64_6'] != $master_bracket->west_r64_6 && $master_bracket->west_r64_6 == NULL)
                                <li class="game game-bottom" onclick="setEntry('west_r32_3', '{{$entries['west_r64_6']}}', '{{$entries['west_r64_5']}}')">{{ $entries['west_r64_6'] }} <span></span></li>
                                @elseif($entries['west_r64_6'] == $master_bracket->west_r64_6 && $master_bracket->west_r64_6 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('west_r32_3', '{{$entries['west_r64_6']}}', '{{$entries['west_r64_5']}}')">{{ $entries['west_r64_6'] }} <span></span></li>
                                @elseif($entries['west_r64_6'] != $master_bracket->west_r64_6 && $master_bracket->west_r64_6 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('west_r32_3', '{{$entries['west_r64_6']}}', '{{$entries['west_r64_5']}}')">{{ $entries['west_r64_6'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['west_r64_7'] == NULL || $entries['west_r64_7'] != $master_bracket->west_r64_7 && $master_bracket->west_r64_7 == NULL)
                                <li class="game game-top" onclick="setEntry('west_r32_4', '{{$entries['west_r64_7']}}', '{{$entries['west_r64_8']}}')">{{ $entries['west_r64_7'] }} <span></span></li>
                                @elseif($entries['west_r64_7'] == $master_bracket->west_r64_7 && $master_bracket->west_r64_7 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('west_r32_4', '{{$entries['west_r64_7']}}', '{{$entries['west_r64_8']}}')">{{ $entries['west_r64_7'] }} <span></span></li>
                                @elseif($entries['west_r64_7'] != $master_bracket->west_r64_7 && $master_bracket->west_r64_7 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('west_r32_4', '{{$entries['west_r64_7']}}', '{{$entries['west_r64_8']}}')">{{ $entries['west_r64_7'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['west_r64_8'] == NULL || $entries['west_r64_8'] != $master_bracket->west_r64_8 && $master_bracket->west_r64_8 == NULL)
                                <li class="game game-bottom" onclick="setEntry('west_r32_4', '{{$entries['west_r64_8']}}', '{{$entries['west_r64_7']}}')">{{ $entries['west_r64_8'] }} <span></span></li>
                                @elseif($entries['west_r64_8'] == $master_bracket->west_r64_8 && $master_bracket->west_r64_8 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('west_r32_4', '{{$entries['west_r64_8']}}', '{{$entries['west_r64_7']}}')">{{ $entries['west_r64_8'] }} <span></span></li>
                                @elseif($entries['west_r64_8'] != $master_bracket->west_r64_8 && $master_bracket->west_r64_8 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('west_r32_4', '{{$entries['west_r64_8']}}', '{{$entries['west_r64_7']}}')">{{ $entries['west_r64_8'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-3">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['west_r32_1'] == NULL || $entries['west_r32_1'] != $master_bracket->west_r32_1 && $master_bracket->west_r32_1 == NULL)
                                <li class="game game-top" onclick="setEntry('west_r16_1', '{{$entries['west_r32_1']}}', '{{$entries['west_r32_2']}}')">{{ $entries['west_r32_1'] }} <span></span></li>
                                @elseif($entries['west_r32_1'] == $master_bracket->west_r32_1 && $master_bracket->west_r32_1 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('west_r16_1', '{{$entries['west_r32_1']}}', '{{$entries['west_r32_2']}}')">{{ $entries['west_r32_1'] }} <span></span></li>
                                @elseif($entries['west_r32_1'] != $master_bracket->west_r32_1 && $master_bracket->west_r32_1 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('west_r16_1', '{{$entries['west_r32_1']}}', '{{$entries['west_r32_2']}}')">{{ $entries['west_r32_1'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['west_r32_2'] == NULL || $entries['west_r32_2'] != $master_bracket->west_r32_2 && $master_bracket->west_r32_2 == NULL)
                                <li class="game game-bottom" onclick="setEntry('west_r16_1', '{{$entries['west_r32_2']}}', '{{$entries['west_r32_1']}}')">{{ $entries['west_r32_2'] }} <span></span></li>
                                @elseif($entries['west_r32_2'] == $master_bracket->west_r32_2 && $master_bracket->west_r32_2 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('west_r16_1', '{{$entries['west_r32_2']}}', '{{$entries['west_r32_1']}}')">{{ $entries['west_r32_2'] }} <span></span></li>
                                @elseif($entries['west_r32_2'] != $master_bracket->west_r32_2 && $master_bracket->west_r32_2 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('west_r16_1', '{{$entries['west_r32_2']}}', '{{$entries['west_r32_1']}}')">{{ $entries['west_r32_2'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['west_r32_3'] == NULL || $entries['west_r32_3'] != $master_bracket->west_r32_3 && $master_bracket->west_r32_3 == NULL)
                                <li class="game game-top" onclick="setEntry('west_r16_2', '{{$entries['west_r32_3']}}', '{{$entries['west_r32_4']}}')">{{ $entries['west_r32_3'] }} <span></span></li>
                                @elseif($entries['west_r32_3'] == $master_bracket->west_r32_3 && $master_bracket->west_r32_3 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('west_r16_2', '{{$entries['west_r32_3']}}', '{{$entries['west_r32_4']}}')">{{ $entries['west_r32_3'] }} <span></span></li>
                                @elseif($entries['west_r32_3'] != $master_bracket->west_r32_3 && $master_bracket->west_r32_3 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('west_r16_2', '{{$entries['west_r32_3']}}', '{{$entries['west_r32_4']}}')">{{ $entries['west_r32_3'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['west_r32_4'] == NULL || $entries['west_r32_4'] != $master_bracket->west_r32_4 && $master_bracket->west_r32_4 == NULL)
                                <li class="game game-bottom" onclick="setEntry('west_r16_2', '{{$entries['west_r32_4']}}', '{{$entries['west_r32_3']}}')">{{ $entries['west_r32_4'] }} <span></span></li>
                                @elseif($entries['west_r32_4'] == $master_bracket->west_r32_4 && $master_bracket->west_r32_4 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('west_r16_2', '{{$entries['west_r32_4']}}', '{{$entries['west_r32_3']}}')">{{ $entries['west_r32_4'] }} <span></span></li>
                                @elseif($entries['west_r32_4'] != $master_bracket->west_r32_4 && $master_bracket->west_r32_4 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('west_r16_2', '{{$entries['west_r32_4']}}', '{{$entries['west_r32_3']}}')">{{ $entries['west_r32_4'] }} <span></span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-4">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['west_r16_1'] == NULL || $entries['west_r16_1'] != $master_bracket->west_r16_1 && $master_bracket->west_r16_1 == NULL)
                                <li class="game game-top" onclick="setEntry('west_r8_1', '{{$entries['west_r16_1']}}', '{{$entries['west_r16_2']}}')">{{ $entries['west_r16_1'] }} <span></span></li>
                                @elseif($entries['west_r16_1'] == $master_bracket->west_r16_1 && $master_bracket->west_r16_1 != NULL)
                                <li class="game game-top green-font" onclick="setEntry('west_r8_1', '{{$entries['west_r16_1']}}', '{{$entries['west_r16_2']}}')">{{ $entries['west_r16_1'] }} <span></span></li>
                                @elseif($entries['west_r16_1'] != $master_bracket->west_r16_1 && $master_bracket->west_r16_1 != NULL)
                                <li class="game game-top red-font" onclick="setEntry('west_r8_1', '{{$entries['west_r16_1']}}', '{{$entries['west_r16_2']}}')">{{ $entries['west_r16_1'] }} <span></span></li>
                                @endif
                                <li class="game game-spacer">&nbsp;</li>
                                @if($entries['west_r16_2'] == NULL || $entries['west_r16_2'] != $master_bracket->west_r16_2 && $master_bracket->west_r16_2 == NULL)
                                <li class="game game-bottom" onclick="setEntry('west_r8_1', '{{$entries['west_r16_2']}}', '{{$entries['west_r16_1']}}')">{{ $entries['west_r16_2'] }} <span></span></li>
                                @elseif($entries['west_r16_2'] == $master_bracket->west_r16_2 && $master_bracket->west_r16_2 != NULL)
                                <li class="game game-bottom green-font" onclick="setEntry('west_r8_1', '{{$entries['west_r16_2']}}', '{{$entries['west_r16_1']}}')">{{ $entries['west_r16_2'] }} <span></span></li>
                                @elseif($entries['west_r16_2'] != $master_bracket->west_r16_2 && $master_bracket->west_r16_2 != NULL)
                                <li class="game game-bottom red-font" onclick="setEntry('west_r8_1', '{{$entries['west_r16_2']}}', '{{$entries['west_r16_1']}}')">{{ $entries['west_r16_2'] }} <span></span></li>
                                @endif
                                
                                <li class="spacer">&nbsp;</li>
                            </ul>       
                        </main>
                    </div>
                    <div class="col-md-6">
                        <main id="tournament-midwest">
                           <ul class="round round-4">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['midwest_r16_1'] == NULL || $entries['midwest_r16_1'] != $master_bracket->midwest_r16_1 && $master_bracket->midwest_r16_1 == NULL)
                                <li class="game-two game-top" onclick="setEntry('midwest_r8_1', '{{$entries['midwest_r16_1']}}', '{{$entries['midwest_r16_2']}}')"> <span>{{ $entries['midwest_r16_1'] }}</span></li>
                                @elseif($entries['midwest_r16_1'] == $master_bracket->midwest_r16_1 && $master_bracket->midwest_r16_1 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('midwest_r8_1', '{{$entries['midwest_r16_1']}}', '{{$entries['midwest_r16_2']}}')"> <span>{{ $entries['midwest_r16_1'] }}</span></li>
                                @elseif($entries['midwest_r16_1'] != $master_bracket->midwest_r16_1 && $master_bracket->midwest_r16_1 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('midwest_r8_1', '{{$entries['midwest_r16_1']}}', '{{$entries['midwest_r16_2']}}')"> <span>{{ $entries['midwest_r16_1'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['midwest_r16_2'] == NULL || $entries['midwest_r16_2'] != $master_bracket->midwest_r16_2 && $master_bracket->midwest_r16_2 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r8_1', '{{$entries['midwest_r16_2']}}', '{{$entries['midwest_r16_1']}}')"> <span>{{ $entries['midwest_r16_2'] }}</span></li>
                                @elseif($entries['midwest_r16_2'] == $master_bracket->midwest_r16_2 && $master_bracket->midwest_r16_2 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('midwest_r8_1', '{{$entries['midwest_r16_2']}}', '{{$entries['midwest_r16_1']}}')"> <span>{{ $entries['midwest_r16_2'] }}</span></li>
                                @elseif($entries['midwest_r16_2'] != $master_bracket->midwest_r16_2 && $master_bracket->midwest_r16_2 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('midwest_r8_1', '{{$entries['midwest_r16_2']}}', '{{$entries['midwest_r16_1']}}')"> <span>{{ $entries['midwest_r16_2'] }}</span></li>
                                @endif
                                
                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-3">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['midwest_r32_1'] == NULL || $entries['midwest_r32_1'] != $master_bracket->midwest_r32_1 && $master_bracket->midwest_r32_1 == NULL)
                                <li class="game-two game-top" onclick="setEntry('midwest_r16_1', '{{$entries['midwest_r32_1']}}', '{{$entries['midwest_r32_2']}}')"> <span>{{ $entries['midwest_r32_1'] }}</span></li>
                                @elseif($entries['midwest_r32_1'] == $master_bracket->midwest_r32_1 && $master_bracket->midwest_r32_1 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('midwest_r16_1', '{{$entries['midwest_r32_1']}}', '{{$entries['midwest_r32_2']}}')"> <span>{{ $entries['midwest_r32_1'] }}</span></li>
                                @elseif($entries['midwest_r32_1'] != $master_bracket->midwest_r32_1 && $master_bracket->midwest_r32_1 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('midwest_r16_1', '{{$entries['midwest_r32_1']}}', '{{$entries['midwest_r32_2']}}')"> <span>{{ $entries['midwest_r32_1'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['midwest_r32_2'] == NULL || $entries['midwest_r32_2'] != $master_bracket->midwest_r32_2 && $master_bracket->midwest_r32_2 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r16_1', '{{$entries['midwest_r32_2']}}', '{{$entries['midwest_r32_1']}}')"> <span>{{ $entries['midwest_r32_2'] }}</span></li>
                                @elseif($entries['midwest_r32_2'] == $master_bracket->midwest_r32_2 && $master_bracket->midwest_r32_2 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('midwest_r16_1', '{{$entries['midwest_r32_2']}}', '{{$entries['midwest_r32_1']}}')"> <span>{{ $entries['midwest_r32_2'] }}</span></li>
                                @elseif($entries['midwest_r32_2'] != $master_bracket->midwest_r32_2 && $master_bracket->midwest_r32_2 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('midwest_r16_1', '{{$entries['midwest_r32_2']}}', '{{$entries['midwest_r32_1']}}')"> <span>{{ $entries['midwest_r32_2'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['midwest_r32_3'] == NULL || $entries['midwest_r32_3'] != $master_bracket->midwest_r32_3 && $master_bracket->midwest_r32_3 == NULL)
                                <li class="game-two game-top" onclick="setEntry('midwest_r16_2', '{{$entries['midwest_r32_3']}}', '{{$entries['midwest_r32_4']}}')"> <span>{{ $entries['midwest_r32_3'] }}</span></li>
                                @elseif($entries['midwest_r32_3'] == $master_bracket->midwest_r32_3 && $master_bracket->midwest_r32_3 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('midwest_r16_2', '{{$entries['midwest_r32_3']}}', '{{$entries['midwest_r32_4']}}')"> <span>{{ $entries['midwest_r32_3'] }}</span></li>
                                @elseif($entries['midwest_r32_3'] != $master_bracket->midwest_r32_3 && $master_bracket->midwest_r32_3 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('midwest_r16_2', '{{$entries['midwest_r32_3']}}', '{{$entries['midwest_r32_4']}}')"> <span>{{ $entries['midwest_r32_3'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['midwest_r32_4'] == NULL || $entries['midwest_r32_4'] != $master_bracket->midwest_r32_4 && $master_bracket->midwest_r32_4 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r16_2', '{{$entries['midwest_r32_4']}}', '{{$entries['midwest_r32_3']}}')"> <span>{{ $entries['midwest_r32_4'] }}</span></li>
                                @elseif($entries['midwest_r32_4'] == $master_bracket->midwest_r32_4 && $master_bracket->midwest_r32_4 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('midwest_r16_2', '{{$entries['midwest_r32_4']}}', '{{$entries['midwest_r32_3']}}')"> <span>{{ $entries['midwest_r32_4'] }}</span></li>
                                @elseif($entries['midwest_r32_4'] != $master_bracket->midwest_r32_4 && $master_bracket->midwest_r32_4 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('midwest_r16_2', '{{$entries['midwest_r32_4']}}', '{{$entries['midwest_r32_3']}}')"> <span>{{ $entries['midwest_r32_4'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-2">
                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['midwest_r64_1'] == NULL || $entries['midwest_r64_1'] != $master_bracket->midwest_r64_1 && $master_bracket->midwest_r64_1 == NULL)
                                <li class="game-two game-top" onclick="setEntry('midwest_r32_1', '{{$entries['midwest_r64_1']}}', '{{$entries['midwest_r64_2']}}')"> <span>{{ $entries['midwest_r64_1'] }}</span></li>
                                @elseif($entries['midwest_r64_1'] == $master_bracket->midwest_r64_1 && $master_bracket->midwest_r64_1 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('midwest_r32_1', '{{$entries['midwest_r64_1']}}', '{{$entries['midwest_r64_2']}}')"> <span>{{ $entries['midwest_r64_1'] }}</span></li>
                                @elseif($entries['midwest_r64_1'] != $master_bracket->midwest_r64_1 && $master_bracket->midwest_r64_1 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('midwest_r32_1', '{{$entries['midwest_r64_1']}}', '{{$entries['midwest_r64_2']}}')"> <span>{{ $entries['midwest_r64_1'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['midwest_r64_2'] == NULL || $entries['midwest_r64_2'] != $master_bracket->midwest_r64_2 && $master_bracket->midwest_r64_2 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r32_1', '{{$entries['midwest_r64_2']}}', '{{$entries['midwest_r64_1']}}')"> <span>{{ $entries['midwest_r64_2'] }}</span></li>
                                @elseif($entries['midwest_r64_2'] == $master_bracket->midwest_r64_2 && $master_bracket->midwest_r64_2 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('midwest_r32_1', '{{$entries['midwest_r64_2']}}', '{{$entries['midwest_r64_1']}}')"> <span>{{ $entries['midwest_r64_2'] }}</span></li>
                                @elseif($entries['midwest_r64_2'] != $master_bracket->midwest_r64_2 && $master_bracket->midwest_r64_2 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('midwest_r32_1', '{{$entries['midwest_r64_2']}}', '{{$entries['midwest_r64_1']}}')"> <span>{{ $entries['midwest_r64_2'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['midwest_r64_3'] == NULL || $entries['midwest_r64_3'] != $master_bracket->midwest_r64_3 && $master_bracket->midwest_r64_3 == NULL)
                                <li class="game-two game-top" onclick="setEntry('midwest_r32_2', '{{$entries['midwest_r64_3']}}', '{{$entries['midwest_r64_4']}}')"> <span>{{ $entries['midwest_r64_3'] }}</span></li>
                                @elseif($entries['midwest_r64_3'] == $master_bracket->midwest_r64_3 && $master_bracket->midwest_r64_3 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('midwest_r32_2', '{{$entries['midwest_r64_3']}}', '{{$entries['midwest_r64_4']}}')"> <span>{{ $entries['midwest_r64_3'] }}</span></li>
                                @elseif($entries['midwest_r64_3'] != $master_bracket->midwest_r64_3 && $master_bracket->midwest_r64_3 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('midwest_r32_2', '{{$entries['midwest_r64_3']}}', '{{$entries['midwest_r64_4']}}')"> <span>{{ $entries['midwest_r64_3'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['midwest_r64_4'] == NULL || $entries['midwest_r64_4'] != $master_bracket->midwest_r64_4 && $master_bracket->midwest_r64_4 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r32_2', '{{$entries['midwest_r64_4']}}', '{{$entries['midwest_r64_3']}}')"> <span>{{ $entries['midwest_r64_4'] }}</span></li>
                                @elseif($entries['midwest_r64_4'] == $master_bracket->midwest_r64_4 && $master_bracket->midwest_r64_4 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('midwest_r32_2', '{{$entries['midwest_r64_4']}}', '{{$entries['midwest_r64_3']}}')"> <span>{{ $entries['midwest_r64_4'] }}</span></li>
                                @elseif($entries['midwest_r64_4'] != $master_bracket->midwest_r64_4 && $master_bracket->midwest_r64_4 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('midwest_r32_2', '{{$entries['midwest_r64_4']}}', '{{$entries['midwest_r64_3']}}')"> <span>{{ $entries['midwest_r64_4'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['midwest_r64_5'] == NULL || $entries['midwest_r64_5'] != $master_bracket->midwest_r64_5 && $master_bracket->midwest_r64_5 == NULL)
                                <li class="game-two game-top" onclick="setEntry('midwest_r32_3', '{{$entries['midwest_r64_5']}}', '{{$entries['midwest_r64_6']}}')"> <span>{{ $entries['midwest_r64_5'] }}</span></li>
                                @elseif($entries['midwest_r64_5'] == $master_bracket->midwest_r64_5 && $master_bracket->midwest_r64_5 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('midwest_r32_3', '{{$entries['midwest_r64_5']}}', '{{$entries['midwest_r64_6']}}')"> <span>{{ $entries['midwest_r64_5'] }}</span></li>
                                @elseif($entries['midwest_r64_5'] != $master_bracket->midwest_r64_5 && $master_bracket->midwest_r64_5 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('midwest_r32_3', '{{$entries['midwest_r64_5']}}', '{{$entries['midwest_r64_6']}}')"> <span>{{ $entries['midwest_r64_5'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['midwest_r64_6'] == NULL || $entries['midwest_r64_6'] != $master_bracket->midwest_r64_6 && $master_bracket->midwest_r64_6 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r32_3', '{{$entries['midwest_r64_6']}}', '{{$entries['midwest_r64_5']}}')"> <span>{{ $entries['midwest_r64_6'] }}</span></li>
                                @elseif($entries['midwest_r64_6'] == $master_bracket->midwest_r64_6 && $master_bracket->midwest_r64_6 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('midwest_r32_3', '{{$entries['midwest_r64_6']}}', '{{$entries['midwest_r64_5']}}')"> <span>{{ $entries['midwest_r64_6'] }}</span></li>
                                @elseif($entries['midwest_r64_6'] != $master_bracket->midwest_r64_6 && $master_bracket->midwest_r64_6 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('midwest_r32_3', '{{$entries['midwest_r64_6']}}', '{{$entries['midwest_r64_5']}}')"> <span>{{ $entries['midwest_r64_6'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                                
                                @if($entries['midwest_r64_7'] == NULL || $entries['midwest_r64_7'] != $master_bracket->midwest_r64_7 && $master_bracket->midwest_r64_7 == NULL)
                                <li class="game-two game-top" onclick="setEntry('midwest_r32_4', '{{$entries['midwest_r64_7']}}', '{{$entries['midwest_r64_8']}}')"> <span>{{ $entries['midwest_r64_7'] }}</span></li>
                                @elseif($entries['midwest_r64_7'] == $master_bracket->midwest_r64_7 && $master_bracket->midwest_r64_7 != NULL)
                                <li class="game-two game-top green-font" onclick="setEntry('midwest_r32_4', '{{$entries['midwest_r64_7']}}', '{{$entries['midwest_r64_8']}}')"> <span>{{ $entries['midwest_r64_7'] }}</span></li>
                                @elseif($entries['midwest_r64_7'] != $master_bracket->midwest_r64_7 && $master_bracket->midwest_r64_7 != NULL)
                                <li class="game-two game-top red-font" onclick="setEntry('midwest_r32_4', '{{$entries['midwest_r64_7']}}', '{{$entries['midwest_r64_8']}}')"> <span>{{ $entries['midwest_r64_7'] }}</span></li>
                                @endif
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                @if($entries['midwest_r64_8'] == NULL || $entries['midwest_r64_8'] != $master_bracket->midwest_r64_8 && $master_bracket->midwest_r64_8 == NULL)
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r32_4', '{{$entries['midwest_r64_8']}}', '{{$entries['midwest_r64_7']}}')"> <span>{{ $entries['midwest_r64_8'] }}</span></li>
                                @elseif($entries['midwest_r64_8'] == $master_bracket->midwest_r64_8 && $master_bracket->midwest_r64_8 != NULL)
                                <li class="game-two game-bottom green-font" onclick="setEntry('midwest_r32_4', '{{$entries['midwest_r64_8']}}', '{{$entries['midwest_r64_7']}}')"> <span>{{ $entries['midwest_r64_8'] }}</span></li>
                                @elseif($entries['midwest_r64_8'] != $master_bracket->midwest_r64_8 && $master_bracket->midwest_r64_8 != NULL)
                                <li class="game-two game-bottom red-font" onclick="setEntry('midwest_r32_4', '{{$entries['midwest_r64_8']}}', '{{$entries['midwest_r64_7']}}')"> <span>{{ $entries['midwest_r64_8'] }}</span></li>
                                @endif

                                <li class="spacer">&nbsp;</li>
                            </ul>
                            <ul class="round round-1">
                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_1', '{{$midwest_teams[0]->name}}', '{{$midwest_teams[15]->name}}')"> <span>({{ $midwest_teams[0]->seed }}){{ $midwest_teams[0]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_1', '{{$midwest_teams[15]->name}}', '{{$midwest_teams[0]->name}}')"> <span>({{ $midwest_teams[15]->seed }}){{ $midwest_teams[15]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_2', '{{$midwest_teams[7]->name}}', '{{$midwest_teams[8]->name}}')"> <span>({{ $midwest_teams[7]->seed }}){{ $midwest_teams[7]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_2', '{{$midwest_teams[8]->name}}', '{{$midwest_teams[7]->name}}')"> <span>({{ $midwest_teams[8]->seed }}){{ $midwest_teams[8]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_3', '{{$midwest_teams[4]->name}}', '{{$midwest_teams[11]->name}}')"> <span>({{ $midwest_teams[4]->seed }}){{ $midwest_teams[4]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_3', '{{$midwest_teams[11]->name}}', '{{$midwest_teams[4]->name}}')"> <span>({{ $midwest_teams[11]->seed }}){{ $midwest_teams[11]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_4', '{{$midwest_teams[3]->name}}', '{{$midwest_teams[12]->name}}')"> <span>({{ $midwest_teams[3]->seed }}){{ $midwest_teams[3]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_4', '{{$midwest_teams[12]->name}}', '{{$midwest_teams[3]->name}}')"> <span>({{ $midwest_teams[12]->seed }}){{ $midwest_teams[12]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_5', '{{$midwest_teams[5]->name}}', '{{$midwest_teams[10]->name}}')"> <span>({{ $midwest_teams[5]->seed }}){{ $midwest_teams[5]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_5', '{{$midwest_teams[10]->name}}', '{{$midwest_teams[5]->name}}')"> <span>({{ $midwest_teams[10]->seed }}){{ $midwest_teams[10]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_6', '{{$midwest_teams[2]->name}}', '{{$midwest_teams[13]->name}}')"> <span>({{ $midwest_teams[2]->seed }}){{ $midwest_teams[2]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_6', '{{$midwest_teams[13]->name}}', '{{$midwest_teams[2]->name}}')"> <span>({{ $midwest_teams[13]->seed }}){{ $midwest_teams[13]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_7', '{{$midwest_teams[6]->name}}', '{{$midwest_teams[9]->name}}')"> <span>({{ $midwest_teams[6]->seed }}){{ $midwest_teams[6]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_7', '{{$midwest_teams[9]->name}}', '{{$midwest_teams[6]->name}}')"> <span>({{ $midwest_teams[9]->seed }}){{ $midwest_teams[9]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                                
                                <li class="game-two game-top" onclick="setEntry('midwest_r64_8', '{{$midwest_teams[1]->name}}', '{{$midwest_teams[14]->name}}')"> <span>({{ $midwest_teams[1]->seed }}){{ $midwest_teams[1]->name }}</span></li>
                                <li class="game-two game-spacer-two">&nbsp;</li>
                                <li class="game-two game-bottom" onclick="setEntry('midwest_r64_8', '{{$midwest_teams[14]->name}}', '{{$midwest_teams[1]->name}}')"> <span>({{ $midwest_teams[14]->seed }}){{ $midwest_teams[14]->name }}</span></li>

                                <li class="spacer">&nbsp;</li>
                            </ul>        
                        </main>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="{{ url('js/index.js') }}"></script>
    <script src="{{ url('js/sweetalert.min.js') }}"></script>

</body>
</html>
                