<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Ras Fam</title>
  
  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="image/R.jpeg">

  
</head>

<body>
    <div id="wrap">
        <div id="regbar">
            <div id="navthing">
                <h2><a href="#" id="loginform">Login</a> | <a id=registerForm href="#">Register</a></h2>
                <div class="login">
                    <div class="arrow-up"></div>
                    <div class="formholder">
                        <div class="randompad">
                            <form method="POST" action="{{ url('/login') }}">
                                <fieldset>
                                    <label name="username">Username</label>
                                    <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus value="{{ old('username') }}">
                                    <label name="password">Password</label>
                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                                    <input type="submit" value="Login" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="register">
                    <div class="arrow-up"></div>
                    <div class="formholder">
                        <div class="randompad">
                            <form method="POST" action="{{ url('/register') }}">
                                <fieldset>
                                    <label for="name">Name</label>
                                    <input name="name" type="text" placeholder="Name" />
                                    <label for="username">Username</label>
                                    <input name="username" type="text" placeholder="Username" />
                                    <label for="email">Email</label>
                                    <input name="email" type="email" placeholder="example@example.com" />
                                    <label for="password">Password</label>
                                    <input name="password" type="password" placeholder="Password" />
                                    <label for="registrationKey">Registration Key</label>
                                    <input name="registrationKey" type="password" placeholder="Registration Key" />
                                    <input type="submit" value="Register" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 style="color:white; text-align: center; margin-top:0px;">Ras Family Pick 'em</h1>
        </div>
        <!-- <div style="width:30%; text-align: center;">
            <img src="image/trophy.jpg">
        </div> -->
        @if(session('error'))
        <div class="col-sm-offset-4 col-sm-4">
            <div class="alert alert-danger" role="alert" style="text-align:center;">
                {{ session('error') }}
            </div>
        </div>
        @endif
    </div>

    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>

</body>
</html>
