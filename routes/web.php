<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Login
//Route::get('/login', ['as' => 'getLogin', 'uses' => 'UserController@getLogin']);
Route::get('/current-user', ['as' => 'currentUser', 'uses' => 'UserController@currentUser']);
Route::post('/login', ['uses' => 'UserController@postLogin']);
Route::post('/register', ['uses' => 'UserController@register']);
Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);

//Route::group(['middleware' => 'auth'], function()
//{
	//Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'MainController@dashboard']);
	Route::get('/tournament-standings', ['as' => 'tournamentStandings', 'uses' => 'MainController@tournamentStandings']);
	Route::get('/my-bracket/{id}', ['as' => 'myBracket', 'uses' => 'MainController@myBracket']);
	Route::post('/set-entry/{round}/team/{team}', ['as' => 'setEntry', 'uses' => 'MainController@setEntry']);
	Route::post('/set-entry-master/{round}/team/{team}', ['as' => 'setEntryMaster', 'uses' => 'MainController@setEntryMaster']);
	Route::post('/lock-bracket', ['as' => 'lockBracket', 'uses' => 'MainController@lockBracket']);
	Route::post('/calculate-points', ['as' => 'calculatePoints', 'uses' => 'MainController@calculatePoints']);
	Route::post('/calculate-possible-points', ['as' => 'calculatePossiblePoints', 'uses' => 'MainController@calculatePossiblePoints']);
	Route::post('/losing-team/{team}', ['as' => 'losingTeam', 'uses' => 'MainController@losingTeam']);
	Route::post('/results-email', ['as' => 'resultsEmail', 'uses' => 'MainController@resultsEmail']);
//});
