<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentBracket extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tournament_brackets';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'owner_id', 'id');
    }

}