<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\TournamentBracket;
use App\Helpers;

class UserController extends Controller
{
    public function getLogin()
	{
		return view('login');
	}

	public function welcome()
	{
		return view('welcome');
	}

	public function postLogin(Request $request)
	{
		if (Auth::attempt([
			'username' => $request->input('username'),
			'password' => $request->input('password')],
			$request->has('remember')
			)){
			$user = Auth::user();
			$user->remember_token = str_random(100);
			$user->save();
			return ['user' => $user, 'api_token' => Helpers::generateWebToken($user->remember_token, $request->getClientIp())];
		}else{
			return response('Login failed', 500);
		}
	}

	public function register(Request $request)
	{
		$username = $request->input('username');
		$first_name = $request->input('first_name');
		$last_name = $request->input('last_name');
		$password = $request->input('password');
		$encrypted_password = Hash::make($password);
		$registration_key = $request->input('registrationKey');

		if($registration_key != 'Rasmussen2018'){
			return response('Incorrect registration key', 500);
		}else{
			$user = new User;
			$user->username = $username;
			$user->first_name = $first_name;
			$user->last_name = $last_name;
			$user->password = $encrypted_password;
			$user->remember_token = str_random(100);
			$user->save();

			$bracket = new TournamentBracket;
			$bracket->username = $username;
			$bracket->owner_id = $user->id;
			$bracket->save();

			$credentials = array(
			    'username' => $request->input('username'),
			    'password' => $request->input('password')
			);

			if (Auth::attempt($credentials)) {
			    return ['user' => $user, 'api_token' => Helpers::generateWebToken($user->remember_token, $request->getClientIp())];
			}
		}
	}

	public function currentUser(Request $request)
    {
        $remember_token = Helpers::extractRememberToken($request->header('Authorization'));
        return response()->json(User::where('remember_token', '=', $remember_token)->first());
    }

	public function logout()
	{
		Auth::logout();
		return redirect()->route('getLogin');
	}
}
