<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mail;

use App\Models\User;
use App\Models\TournamentBracket;
use App\Models\TournamentTeam;
use App\Models\SmsGateway;

class MainController extends Controller
{
    public function dashboard()
	{
		return view('dashboard');
	}

	public function tournamentStandings()
	{
		$brackets = TournamentBracket::with('user')->where('username', '!=', 'master')->where('submitted', 1)->orderBy('points', 'desc')->get();

		return response()->json($brackets);
	}

	public function myBracket($id)
	{
		$array = [];
		$entries = TournamentBracket::where('owner_id', $id)->first();
		$master_bracket = TournamentBracket::where('username', 'master')->first();
		$south_teams = TournamentTeam::where('division', 'South')->get();
		$east_teams = TournamentTeam::where('division', 'East')->get();
		$west_teams = TournamentTeam::where('division', 'West')->get();
		$midwest_teams = TournamentTeam::where('division', 'Midwest')->get();

		$array['entries'] = $entries;
		$array['master_bracket'] = $master_bracket;
		$array['south_teams'] = $south_teams;
		$array['east_teams'] = $east_teams;
		$array['west_teams'] = $west_teams;
		$array['midwest_teams'] = $midwest_teams;

		return response()->json($array);

		//return view('my-bracket', ['entries' => $entries, 'south_teams' => $south_teams, 'east_teams' => $east_teams, 'west_teams' => $west_teams, 'midwest_teams' => $midwest_teams, 'master_bracket' => $master_bracket]);
	}

	public function setEntry(Request $request, $round, $team)
	{
		$owner_id = $request->input('owner_id');
		$bracket = TournamentBracket::where('owner_id', $owner_id)->first();
		$team_info = TournamentTeam::where('name', $team)->first();
		
        if($bracket){
        	if($bracket->submitted == 1){
        		return response()->json('Locked');
        	}else{
        		$bracket->$round = $team;
            	$bracket->save();
        	}
        }else{
            $this->newEntry($round, $team, $owner_id);
        }

        return $this->myBracket($owner_id);
	}

	public function setEntryMaster($round, $team)
	{
		$bracket = TournamentBracket::where('username', 'master')->first();
		$bracket->$round = $team;
        $bracket->save();

        return response()->json($bracket);
	}

	public function newEntry(Request $request, $round, $team, $owner)
    {
        $user = $request->input('username');

        $bracket = new TournamentBracket;
        $bracket->username = $user;
        $bracket->owner_id = $owner;
        $bracket->submitted = 0;
        $bracket->$round = $team;
        $bracket->save();

        return response()->json($bracket);
    }

    public function lockBracket(Request $request)
    {
        $user = $request->input('owner_id');

        $bracket = TournamentBracket::where('owner_id', $user)->first();
        $bracket->submitted = 1;
        $bracket->save();

        return $this->myBracket($user);
    }

    public function losingTeam($team)
    {
        $tournament_team = TournamentTeam::where('name', $team)->first();
        $tournament_team->alive = 0;
        $tournament_team->save();

        return response()->json('success');
    }

    public function resultsEmail()
    {
        $brackets = TournamentBracket::where('username', '!=', 'master')->orderBy('points', 'desc')->get();
        foreach($brackets as $bracket){
        	$user = User::where('username', $bracket->username)->first();
        	//Send SMS notification via email.
            $email_address = $this->constructSMSEmail($user->sms_number, $user->sms_carrier);

            $email_data = ['first_name' => $user->name, 'brackets' => $brackets];

            try {
	            Mail::send('emails.sms', $email_data, function ($message) use ($email_address) {
		            $message->to($email_address);
		        });
	        } catch (Exception $e) {
	        	return $e;
	            return response('Email notification failed to send  ', 500);
	        }        

        }

        return response()->json('success');
    }

    public function constructSMSEmail($number, $carrier) {
        $gateway = SmsGateway::where("carrier", $carrier)->first();
        return $number . "@" . $gateway->email_to_sms_gateway;
    }

    public function calculatePoints()
    {
        $submitted_brackets = TournamentBracket::where('username', '!=', 'master')->where('submitted', 1)->get();
        foreach($submitted_brackets as $user_bracket){
        	$user_bracket->points = 0;
	        $user_bracket->save();
	        $master_bracket = TournamentBracket::where('username', 'master')->first();
	        $points = 0;

	        if($user_bracket->south_r64_1 == $master_bracket->south_r64_1 && $master_bracket->south_r64_1 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->south_r64_2 == $master_bracket->south_r64_2 && $master_bracket->south_r64_2 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->south_r64_3 == $master_bracket->south_r64_3 && $master_bracket->south_r64_3 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->south_r64_4 == $master_bracket->south_r64_4 && $master_bracket->south_r64_4 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->south_r64_5 == $master_bracket->south_r64_5 && $master_bracket->south_r64_5 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->south_r64_6 == $master_bracket->south_r64_6 && $master_bracket->south_r64_6 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->south_r64_7 == $master_bracket->south_r64_7 && $master_bracket->south_r64_7 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->south_r64_8 == $master_bracket->south_r64_8 && $master_bracket->south_r64_8 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_1 == $master_bracket->east_r64_1 && $master_bracket->east_r64_1 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_2 == $master_bracket->east_r64_2 && $master_bracket->east_r64_2 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_3 == $master_bracket->east_r64_3 && $master_bracket->east_r64_3 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_4 == $master_bracket->east_r64_4 && $master_bracket->east_r64_4 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_5 == $master_bracket->east_r64_5 && $master_bracket->east_r64_5 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_6 == $master_bracket->east_r64_6 && $master_bracket->east_r64_6 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_7 == $master_bracket->east_r64_7 && $master_bracket->east_r64_7 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->east_r64_8 == $master_bracket->east_r64_8 && $master_bracket->east_r64_8 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_1 == $master_bracket->west_r64_1 && $master_bracket->west_r64_1 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_2 == $master_bracket->west_r64_2 && $master_bracket->west_r64_2 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_3 == $master_bracket->west_r64_3 && $master_bracket->west_r64_3 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_4 == $master_bracket->west_r64_4 && $master_bracket->west_r64_4 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_5 == $master_bracket->west_r64_5 && $master_bracket->west_r64_5 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_6 == $master_bracket->west_r64_6 && $master_bracket->west_r64_6 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_7 == $master_bracket->west_r64_7 && $master_bracket->west_r64_7 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->west_r64_8 == $master_bracket->west_r64_8 && $master_bracket->west_r64_8 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_1 == $master_bracket->midwest_r64_1 && $master_bracket->midwest_r64_1 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_2 == $master_bracket->midwest_r64_2 && $master_bracket->midwest_r64_2 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_3 == $master_bracket->midwest_r64_3 && $master_bracket->midwest_r64_3 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_4 == $master_bracket->midwest_r64_4 && $master_bracket->midwest_r64_4 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_5 == $master_bracket->midwest_r64_5 && $master_bracket->midwest_r64_5 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_6 == $master_bracket->midwest_r64_6 && $master_bracket->midwest_r64_6 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_7 == $master_bracket->midwest_r64_7 && $master_bracket->midwest_r64_7 != NULL){
	            $points = $points + 10;
	        }
	        if($user_bracket->midwest_r64_8 == $master_bracket->midwest_r64_8 && $master_bracket->midwest_r64_8 != NULL){
	            $points = $points + 10;
	        }
	        // 2nd Round Scoring
	        if($user_bracket->south_r32_1 == $master_bracket->south_r32_1 && $master_bracket->south_r32_1 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->south_r32_2 == $master_bracket->south_r32_2 && $master_bracket->south_r32_2 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->south_r32_3 == $master_bracket->south_r32_3 && $master_bracket->south_r32_3 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->south_r32_4 == $master_bracket->south_r32_4 && $master_bracket->south_r32_4 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->east_r32_1 == $master_bracket->east_r32_1 && $master_bracket->east_r32_1 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->east_r32_2 == $master_bracket->east_r32_2 && $master_bracket->east_r32_2 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->east_r32_3 == $master_bracket->east_r32_3 && $master_bracket->east_r32_3 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->east_r32_4 == $master_bracket->east_r32_4 && $master_bracket->east_r32_4 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->west_r32_1 == $master_bracket->west_r32_1 && $master_bracket->west_r32_1 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->west_r32_2 == $master_bracket->west_r32_2 && $master_bracket->west_r32_2 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->west_r32_3 == $master_bracket->west_r32_3 && $master_bracket->west_r32_3 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->west_r32_4 == $master_bracket->west_r32_4 && $master_bracket->west_r32_4 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->midwest_r32_1 == $master_bracket->midwest_r32_1 && $master_bracket->midwest_r32_1 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->midwest_r32_2 == $master_bracket->midwest_r32_2 && $master_bracket->midwest_r32_2 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->midwest_r32_3 == $master_bracket->midwest_r32_3 && $master_bracket->midwest_r32_3 != NULL){
	            $points = $points + 20;
	        }
	        if($user_bracket->midwest_r32_4 == $master_bracket->midwest_r32_4 && $master_bracket->midwest_r32_4 != NULL){
	            $points = $points + 20;
	        }
	        // Round 3 Scoring
	        if($user_bracket->south_r16_1 == $master_bracket->south_r16_1 && $master_bracket->south_r16_1 != NULL){
	            $points = $points + 40;
	        }
	        if($user_bracket->south_r16_2 == $master_bracket->south_r16_2 && $master_bracket->south_r16_2 != NULL){
	            $points = $points + 40;
	        }
	        if($user_bracket->east_r16_1 == $master_bracket->east_r16_1 && $master_bracket->east_r16_1 != NULL){
	            $points = $points + 40;
	        }
	        if($user_bracket->east_r16_2 == $master_bracket->east_r16_2 && $master_bracket->east_r16_2 != NULL){
	            $points = $points + 40;
	        }
	        if($user_bracket->west_r16_1 == $master_bracket->west_r16_1 && $master_bracket->west_r16_1 != NULL){
	            $points = $points + 40;
	        }
	        if($user_bracket->west_r16_2 == $master_bracket->west_r16_2 && $master_bracket->west_r16_2 != NULL){
	            $points = $points + 40;
	        }
	        if($user_bracket->midwest_r16_1 == $master_bracket->midwest_r16_1 && $master_bracket->midwest_r16_1 != NULL){
	            $points = $points + 40;
	        }
	        if($user_bracket->midwest_r16_2 == $master_bracket->midwest_r16_2 && $master_bracket->midwest_r16_2 != NULL){
	            $points = $points + 40;
	        }
	        // Round 4 Scoring
	        if($user_bracket->south_r8_1 == $master_bracket->south_r8_1 && $master_bracket->south_r8_1 != NULL){
	            $points = $points + 80;
	        }
	        if($user_bracket->east_r8_1 == $master_bracket->east_r8_1 && $master_bracket->east_r8_1 != NULL){
	            $points = $points + 80;
	        }
	        if($user_bracket->west_r8_1 == $master_bracket->west_r8_1 && $master_bracket->west_r8_1 != NULL){
	            $points = $points + 80;
	        }
	        if($user_bracket->midwest_r8_1 == $master_bracket->midwest_r8_1 && $master_bracket->midwest_r8_1 != NULL){
	            $points = $points + 80;
	        }
	        // Round 5 Scoring
	        if($user_bracket->left_champ == $master_bracket->left_champ && $master_bracket->left_champ != NULL){
	            $points = $points + 160;
	        }
	        if($user_bracket->right_champ == $master_bracket->right_champ && $master_bracket->right_champ != NULL){
	            $points = $points + 160;
	        }
	        // Championship Scoring
	        if($user_bracket->champion == $master_bracket->champion && $master_bracket->champion != NULL){
	            $points = $points + 320;
	        }

	        $user_bracket->points = $points;
	        $user_bracket->save();
        }

        return response()->json('success');
    }

    public function calculatePossiblePoints()
    {
        //$user_bracket = TournamentBracket::where('username', $uname)->first();
        $active_brackets = TournamentBracket::where('submitted', 1)->where('username', '!=', 'master')->get();

        foreach($active_brackets as $user_bracket){
            $user_bracket->possible_points = 0;
            $user_bracket->save();
            $master_bracket = TournamentBracket::where('username', 'master')->first();
            $points = 0;
            if($user_bracket->south_r64_1 != NULL && $master_bracket->south_r64_1 == NULL){
                $south_r64_1_team = TournamentTeam::where('name', $user_bracket->south_r64_1)->first();
                if($south_r64_1_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->south_r64_2 != NULL && $master_bracket->south_r64_2 == NULL){
                $south_r64_2_team = TournamentTeam::where('name', $user_bracket->south_r64_2)->first();
                if($south_r64_2_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->south_r64_3 != NULL && $master_bracket->south_r64_3 == NULL){
                $south_r64_3_team = TournamentTeam::where('name', $user_bracket->south_r64_3)->first();
                if($south_r64_3_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->south_r64_4 != NULL && $master_bracket->south_r64_4 == NULL){
                $south_r64_4_team = TournamentTeam::where('name', $user_bracket->south_r64_4)->first();
                if($south_r64_4_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->south_r64_5 != NULL && $master_bracket->south_r64_5 == NULL){
                $south_r64_5_team = TournamentTeam::where('name', $user_bracket->south_r64_5)->first();
                if($south_r64_5_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->south_r64_6 != NULL && $master_bracket->south_r64_6 == NULL){
                $south_r64_6_team = TournamentTeam::where('name', $user_bracket->south_r64_6)->first();
                if($south_r64_6_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->south_r64_7 != NULL && $master_bracket->south_r64_7 == NULL){
                $south_r64_7_team = TournamentTeam::where('name', $user_bracket->south_r64_7)->first();
                if($south_r64_7_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->south_r64_8 != NULL && $master_bracket->south_r64_8 == NULL){
                $south_r64_8_team = TournamentTeam::where('name', $user_bracket->south_r64_8)->first();
                if($south_r64_8_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_1 != NULL && $master_bracket->east_r64_1 == NULL){
                $east_r64_1_team = TournamentTeam::where('name', $user_bracket->east_r64_1)->first();
                if($east_r64_1_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_2 != NULL && $master_bracket->east_r64_2 == NULL){
                $east_r64_2_team = TournamentTeam::where('name', $user_bracket->east_r64_2)->first();
                if($east_r64_2_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_3 != NULL && $master_bracket->east_r64_3 == NULL){
                $east_r64_3_team = TournamentTeam::where('name', $user_bracket->east_r64_3)->first();
                if($east_r64_3_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_4 != NULL && $master_bracket->east_r64_4 == NULL){
                $east_r64_4_team = TournamentTeam::where('name', $user_bracket->east_r64_4)->first();
                if($east_r64_4_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_5 != NULL && $master_bracket->east_r64_5 == NULL){
                $east_r64_5_team = TournamentTeam::where('name', $user_bracket->east_r64_5)->first();
                if($east_r64_5_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_6 != NULL && $master_bracket->east_r64_6 == NULL){
                $east_r64_6_team = TournamentTeam::where('name', $user_bracket->east_r64_6)->first();
                if($east_r64_6_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_7 != NULL && $master_bracket->east_r64_7 == NULL){
                $east_r64_7_team = TournamentTeam::where('name', $user_bracket->east_r64_7)->first();
                if($east_r64_7_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->east_r64_8 != NULL && $master_bracket->east_r64_8 == NULL){
                $east_r64_8_team = TournamentTeam::where('name', $user_bracket->east_r64_8)->first();
                if($east_r64_8_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_1 != NULL && $master_bracket->west_r64_1 == NULL){
                $west_r64_1_team = TournamentTeam::where('name', $user_bracket->west_r64_1)->first();
                if($west_r64_1_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_2 != NULL && $master_bracket->west_r64_2 == NULL){
                $west_r64_2_team = TournamentTeam::where('name', $user_bracket->west_r64_2)->first();
                if($west_r64_2_team['alive'] == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_3 != NULL && $master_bracket->west_r64_3 == NULL){
                $west_r64_3_team = TournamentTeam::where('name', $user_bracket->west_r64_3)->first();
                if($west_r64_3_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_4 != NULL && $master_bracket->west_r64_4 == NULL){
                $west_r64_4_team = TournamentTeam::where('name', $user_bracket->west_r64_4)->first();
                if($west_r64_4_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_5 != NULL && $master_bracket->west_r64_5 == NULL){
                $west_r64_5_team = TournamentTeam::where('name', $user_bracket->west_r64_5)->first();
                if($west_r64_5_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_6 != NULL && $master_bracket->west_r64_6 == NULL){
                $west_r64_6_team = TournamentTeam::where('name', $user_bracket->west_r64_6)->first();
                if($west_r64_6_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_7 != NULL && $master_bracket->west_r64_7 == NULL){
                $west_r64_7_team = TournamentTeam::where('name', $user_bracket->west_r64_7)->first();
                if($west_r64_7_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->west_r64_8 != NULL && $master_bracket->west_r64_8 == NULL){
                $west_r64_8_team = TournamentTeam::where('name', $user_bracket->west_r64_8)->first();
                if($west_r64_8_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_1 != NULL && $master_bracket->midwest_r64_1 == NULL){
                $midwest_r64_1_team = TournamentTeam::where('name', $user_bracket->midwest_r64_1)->first();
                if($midwest_r64_1_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_2 != NULL && $master_bracket->midwest_r64_2 == NULL){
                $midwest_r64_2_team = TournamentTeam::where('name', $user_bracket->midwest_r64_2)->first();
                if($midwest_r64_2_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_3 != NULL && $master_bracket->midwest_r64_3 == NULL){
                $midwest_r64_3_team = TournamentTeam::where('name', $user_bracket->midwest_r64_3)->first();
                if($midwest_r64_3_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_4 != NULL && $master_bracket->midwest_r64_4 == NULL){
                $midwest_r64_4_team = TournamentTeam::where('name', $user_bracket->midwest_r64_4)->first();
                if($midwest_r64_4_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_5 != NULL && $master_bracket->midwest_r64_5 == NULL){
                $midwest_r64_5_team = TournamentTeam::where('name', $user_bracket->midwest_r64_5)->first();
                if($midwest_r64_5_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_6 != NULL && $master_bracket->midwest_r64_6 == NULL){
                $midwest_r64_6_team = TournamentTeam::where('name', $user_bracket->midwest_r64_6)->first();
                if($midwest_r64_6_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_7 != NULL && $master_bracket->midwest_r64_7 == NULL){
                $midwest_r64_7_team = TournamentTeam::where('name', $user_bracket->midwest_r64_7)->first();
                if($midwest_r64_7_team->alive == 1){
                    $points = $points + 10;
                }
            }
            if($user_bracket->midwest_r64_8 != NULL && $master_bracket->midwest_r64_8 == NULL){
                $midwest_r64_8_team = TournamentTeam::where('name', $user_bracket->midwest_r64_8)->first();
                if($midwest_r64_8_team->alive == 1){
                    $points = $points + 10;
                }
            }
            // 2nd Round Scoring
            if($user_bracket->south_r32_1 != NULL && $master_bracket->south_r32_1 == NULL){
                $south_r32_1_team = TournamentTeam::where('name', $user_bracket->south_r32_1)->first();
                if($south_r32_1_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->south_r32_2 != NULL && $master_bracket->south_r32_2 == NULL){
                $south_r32_2_team = TournamentTeam::where('name', $user_bracket->south_r32_2)->first();
                if($south_r32_2_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->south_r32_3 != NULL && $master_bracket->south_r32_3 == NULL){
                $south_r32_3_team = TournamentTeam::where('name', $user_bracket->south_r32_3)->first();
                if($south_r32_3_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->south_r32_4 != NULL && $master_bracket->south_r32_4 == NULL){
                $south_r32_4_team = TournamentTeam::where('name', $user_bracket->south_r32_4)->first();
                if($south_r32_4_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->east_r32_1 != NULL && $master_bracket->east_r32_1 == NULL){
                $east_r32_1_team = TournamentTeam::where('name', $user_bracket->east_r32_1)->first();
                if($east_r32_1_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->east_r32_2 != NULL && $master_bracket->east_r32_2 == NULL){
                $east_r32_2_team = TournamentTeam::where('name', $user_bracket->east_r32_2)->first();
                if($east_r32_2_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->east_r32_3 != NULL && $master_bracket->east_r32_3 == NULL){
                $east_r32_3_team = TournamentTeam::where('name', $user_bracket->east_r32_3)->first();
                if($east_r32_3_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->east_r32_4 != NULL && $master_bracket->east_r32_4 == NULL){
                $east_r32_4_team = TournamentTeam::where('name', $user_bracket->east_r32_4)->first();
                if($east_r32_4_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->west_r32_1 != NULL && $master_bracket->west_r32_1 == NULL){
                $west_r32_1_team = TournamentTeam::where('name', $user_bracket->west_r32_1)->first();
                if($west_r32_1_team['alive'] == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->west_r32_2 != NULL && $master_bracket->west_r32_2 == NULL){
                $west_r32_2_team = TournamentTeam::where('name', $user_bracket->west_r32_2)->first();
                if($west_r32_2_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->west_r32_3 != NULL && $master_bracket->west_r32_3 == NULL){
                $west_r32_3_team = TournamentTeam::where('name', $user_bracket->west_r32_3)->first();
                if($west_r32_3_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->west_r32_4 != NULL && $master_bracket->west_r32_4 == NULL){
                $west_r32_4_team = TournamentTeam::where('name', $user_bracket->west_r32_4)->first();
                if($west_r32_4_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->midwest_r32_1 != NULL && $master_bracket->midwest_r32_1 == NULL){
                $midwest_r32_1_team = TournamentTeam::where('name', $user_bracket->midwest_r32_1)->first();
                if($midwest_r32_1_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->midwest_r32_2 != NULL && $master_bracket->midwest_r32_2 == NULL){
                $midwest_r32_2_team = TournamentTeam::where('name', $user_bracket->midwest_r32_2)->first();
                if($midwest_r32_2_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->midwest_r32_3 != NULL && $master_bracket->midwest_r32_3 == NULL){
                $midwest_r32_3_team = TournamentTeam::where('name', $user_bracket->midwest_r32_3)->first();
                if($midwest_r32_3_team->alive == 1){
                    $points = $points + 20;
                }
            }
            if($user_bracket->midwest_r32_4 != NULL && $master_bracket->midwest_r32_4 == NULL){
                $midwest_r32_4_team = TournamentTeam::where('name', $user_bracket->midwest_r32_4)->first();
                if($midwest_r32_4_team->alive == 1){
                    $points = $points + 20;
                }
            }
            // Round 3 Scoring
            if($user_bracket->south_r16_1 != NULL && $master_bracket->south_r16_1 == NULL){
                $south_r16_1_team = TournamentTeam::where('name', $user_bracket->south_r16_1)->first();
                if($south_r16_1_team->alive == 1){
                    $points = $points + 40;
                }
            }
            if($user_bracket->south_r16_2 != NULL && $master_bracket->south_r16_2 == NULL){
                $south_r16_2_team = TournamentTeam::where('name', $user_bracket->south_r16_2)->first();
                if($south_r16_2_team->alive == 1){
                    $points = $points + 40;
                }
            }
            if($user_bracket->east_r16_1 != NULL && $master_bracket->east_r16_1 == NULL){
                $east_r16_1_team = TournamentTeam::where('name', $user_bracket->east_r16_1)->first();
                if($east_r16_1_team->alive == 1){
                    $points = $points + 40;
                }
            }
            if($user_bracket->east_r16_2 != NULL && $master_bracket->east_r16_2 == NULL){
                $east_r16_2_team = TournamentTeam::where('name', $user_bracket->east_r16_2)->first();
                if($east_r16_2_team->alive == 1){
                    $points = $points + 40;
                }
            }
            if($user_bracket->west_r16_1 != NULL && $master_bracket->west_r16_1 == NULL){
                $west_r16_1_team = TournamentTeam::where('name', $user_bracket->west_r16_1)->first();
                if($west_r16_1_team->alive == 1){
                    $points = $points + 40;
                }
            }
            if($user_bracket->west_r16_2 != NULL && $master_bracket->west_r16_2 == NULL){
                $west_r16_2_team = TournamentTeam::where('name', $user_bracket->west_r16_2)->first();
                if($west_r16_2_team->alive == 1){
                    $points = $points + 40;
                }
            }
            if($user_bracket->midwest_r16_1 != NULL && $master_bracket->midwest_r16_1 == NULL){
                $midwest_r16_1_team = TournamentTeam::where('name', $user_bracket->midwest_r16_1)->first();
                if($midwest_r16_1_team->alive == 1){
                    $points = $points + 40;
                }
            }
            if($user_bracket->midwest_r16_2 != NULL && $master_bracket->midwest_r16_2 == NULL){
                $midwest_r16_2_team = TournamentTeam::where('name', $user_bracket->midwest_r16_2)->first();
                if($midwest_r16_2_team->alive == 1){
                    $points = $points + 40;
                }
            }
            // Round 4 Scoring
            if($user_bracket->south_r8_1 != NULL && $master_bracket->south_r8_1 == NULL){
                $south_r8_1_team = TournamentTeam::where('name', $user_bracket->south_r8_1)->first();
                if($south_r8_1_team->alive == 1){
                    $points = $points + 80;
                }
            }
            if($user_bracket->east_r8_1 != NULL && $master_bracket->east_r8_1 == NULL){
                $east_r8_1_team = TournamentTeam::where('name', $user_bracket->east_r8_1)->first();
                if($east_r8_1_team->alive == 1){
                    $points = $points + 80;
                }
            }
            if($user_bracket->west_r8_1 != NULL && $master_bracket->west_r8_1 == NULL){
                $west_r8_1_team = TournamentTeam::where('name', $user_bracket->west_r8_1)->first();
                if($west_r8_1_team->alive == 1){
                    $points = $points + 80;
                }
            }
            if($user_bracket->midwest_r8_1 != NULL && $master_bracket->midwest_r8_1 == NULL){
                $midwest_r8_1_team = TournamentTeam::where('name', $user_bracket->midwest_r8_1)->first();
                if($midwest_r8_1_team->alive == 1){
                    $points = $points + 80;
                }
            }
            // Round 5 Scoring
            if($user_bracket->left_champ != NULL && $master_bracket->left_champ == NULL){
                $left_champ_team = TournamentTeam::where('name', $user_bracket->left_champ)->first();
                if($left_champ_team->alive == 1){
                    $points = $points + 160;
                }
            }
            if($user_bracket->right_champ != NULL && $master_bracket->right_champ == NULL){
                $right_champ_team = TournamentTeam::where('name', $user_bracket->right_champ)->first();
                if($right_champ_team->alive == 1){
                    $points = $points + 160;
                }
            }
            // Championship Scoring
            if($user_bracket->champion != NULL && $master_bracket->champion == NULL){
                $champion_team = TournamentTeam::where('name', $user_bracket->champion)->first();
                if($champion_team->alive == 1){
                    $points = $points + 320;
                }
            }

            $user_bracket->possible_points = $points;
            $user_bracket->save();
        }

        return response()->json('success');
    } 
}
