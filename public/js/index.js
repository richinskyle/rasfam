$('input[type="submit"]').mousedown(function(){
  $(this).css('background', '#2ecc71');
});
$('input[type="submit"]').mouseup(function(){
  $(this).css('background', '#1abc9c');
});

$('#loginform').click(function(){
  $('.login').fadeToggle('slow');
  $(this).toggleClass('green');
});

$('#registerForm').click(function(){
  $('.register').fadeToggle('slow');
  $(this).toggleClass('green');
});



$(document).mouseup(function (e)
{
    var container = $(".login");
    var registerContainer = $(".register");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
        $('#loginform').removeClass('green');
    }

    if(!registerContainer.is(e.target) && registerContainer.has(e.target).length === 0){
        registerContainer.hide();
        $('#registerForm').removeClass('green');
    }
});

function setEntry(round, team, losing_team) {
  if(window.location.href.indexOf("master") > -1) {
    $.ajax({
        url: '/set-entry-master/' + round + '/team/' + team,
        type: "post",
        data: {'_token': $('input[name=_token]').val()},
        success: function(data){
            $.ajax({
              url: '/losing-team/' + losing_team,
              type: "post",
              data: {'_token': $('input[name=_token]').val()},
              success: function(data){
                  $("#content").load(location.href + " #content");
              },
              error: function(data) { 
                  swal('Error!', 'Please contact the site administrator', 'error');
              }
          });
        },
        error: function(data) { 
            swal('Error!', 'Please contact the site administrator', 'error');
        }
    });
  }else{
    $.ajax({
        url: '/set-entry/' + round + '/team/' + team,
        type: "post",
        data: {'_token': $('input[name=_token]').val()},
        success: function(data){
            if(data === 'Locked'){
              swal('Sorry!', "What's done is done :)", 'warning');
            }else{
              $("#content").load(location.href + " #content");
            }
        },
        error: function(data) { 
            swal('Error!', 'Please contact the site administrator', 'error');
        }
    });
  }
}

function lockBracket() {
  swal({
    title: "Are you sure?",
    text: "You cannot make changes to your bracket once it is locked!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Lock it!",
    cancelButtonText: "Not Yet!",
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    closeOnCancel: false
  },
  function(isConfirm){
    if (isConfirm) {
          $.ajax({
            url: '/lock-bracket',
            type: "post",
            data: {'_token': $('input[name=_token]').val()},
            success: function(data){
                $("#content").load(location.href + " #content");
                swal("Success!", "You have submitted your bracket", "success");
            },
            error: function(data) { 
                alert('Error');
            }
        });
    } else {
      swal("Cancelled", "You may edit your bracket", "error");
    }
  });
}

function submitBracketName() {
  $.ajax({
      url: '/submit-bracket-name',
      type: "post",
      data: {'_token': $('input[name=_token]').val(), 'name': $('input[name=bracketName]').val()},
      success: function(data){
          $("#content").load(location.href + " #content");
      },
      error: function(data) { 
          alert('Error');
      }
  });
}

function resultsEmail() {
  $.ajax({
      url: '/results-email',
      type: "post",
      data: {'_token': $('input[name=_token]').val()},
      success: function(data){
          alert('Success');
      },
      error: function(data) { 
          alert('Error');
      }
  });
}

function calculatePoints() {
  //var user = $('input[name=username]').val();
  $.ajax({
      url: '/calculate-points',
      type: "post",
      data: {'_token': $('input[name=_token]').val()},
      success: function(data){
        $.ajax({
            url: '/calculate-possible-points',
            type: "post",
            data: {'_token': $('input[name=_token]').val()},
            success: function(data){
                $("#content").load(location.href + " #content");
            },
            error: function(data) { 
                //alert('Error');
            }
        });
      },
      error: function(data) { 
          //alert('Error');
      }
  });
}