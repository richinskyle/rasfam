<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentBracketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_brackets', function ($table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('bracket_name', 50);
            $table->integer('submitted');
            $table->string('south_r64_1', 50);
            $table->string('south_r64_2', 50);
            $table->string('south_r64_3', 50);
            $table->string('south_r64_4', 50);
            $table->string('south_r64_5', 50);
            $table->string('south_r64_6', 50);
            $table->string('south_r64_7', 50);
            $table->string('south_r64_8', 50);
            $table->string('east_r64_1', 50);
            $table->string('east_r64_2', 50);
            $table->string('east_r64_3', 50);
            $table->string('east_r64_4', 50);
            $table->string('east_r64_5', 50);
            $table->string('east_r64_6', 50);
            $table->string('east_r64_7', 50);
            $table->string('east_r64_8', 50);
            $table->string('west_r64_1', 50);
            $table->string('west_r64_2', 50);
            $table->string('west_r64_3', 50);
            $table->string('west_r64_4', 50);
            $table->string('west_r64_5', 50);
            $table->string('west_r64_6', 50);
            $table->string('west_r64_7', 50);
            $table->string('west_r64_8', 50);
            $table->string('midwest_r64_1', 50);
            $table->string('midwest_r64_2', 50);
            $table->string('midwest_r64_3', 50);
            $table->string('midwest_r64_4', 50);
            $table->string('midwest_r64_5', 50);
            $table->string('midwest_r64_6', 50);
            $table->string('midwest_r64_7', 50);
            $table->string('midwest_r64_8', 50);
            $table->string('south_r32_1', 50);
            $table->string('south_r32_2', 50);
            $table->string('south_r32_3', 50);
            $table->string('south_r32_4', 50);
            $table->string('east_r32_1', 50);
            $table->string('east_r32_2', 50);
            $table->string('east_r32_3', 50);
            $table->string('east_r32_4', 50);
            $table->string('west_r32_1', 50);
            $table->string('west_r32_2', 50);
            $table->string('west_r32_3', 50);
            $table->string('west_r32_4', 50);
            $table->string('midwest_r32_1', 50);
            $table->string('midwest_r32_2', 50);
            $table->string('midwest_r32_3', 50);
            $table->string('midwest_r32_4', 50);
            $table->string('south_r16_1', 50);
            $table->string('south_r16_2', 50);
            $table->string('east_r16_1', 50);
            $table->string('east_r16_2', 50);
            $table->string('west_r16_1', 50);
            $table->string('west_r16_2', 50);
            $table->string('midwest_r16_1', 50);
            $table->string('midwest_r16_2', 50);
            $table->string('south_r8_1', 50);
            $table->string('east_r8_1', 50);
            $table->string('west_r8_1', 50);
            $table->string('midwest_r8_1', 50);
            $table->string('left_champ', 50);
            $table->string('right_champ', 50);
            $table->string('champion', 50);
            $table->timestamps();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_brackets');
    }
}
